package kr.co.gwh.beauteach.domain;

import java.util.List;

public class Videos {

	private int video_no;
	private String url;
	private String video_id;
	private String title;
	private String description;
	private String date;
	private int category;
	private String category_name;
	private int teacher_no;
	private String thumbnails;
	
	private int starting_point;
	
	private String teacher_id;
	private String teacher_name;
	private String teacher_img;
	
	private String itemIdArr;
	
	private String iframeUrl;
	
	private List<Items> itemList;
	
	public int getVideo_no() {
		return video_no;
	}
	public void setVideo_no(int video_no) {
		this.video_no = video_no;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getVideo_id() {
		return video_id;
	}
	public void setVideo_id(String video_id) {
		this.video_id = video_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public int getTeacher_no() {
		return teacher_no;
	}
	public void setTeacher_no(int teacher_no) {
		this.teacher_no = teacher_no;
	}
	public String getThumbnails() {
		return thumbnails;
	}
	public void setThumbnail(String thumbnails) {
		this.thumbnails = thumbnails;
	}
	public List<Items> getItemList() {
		return itemList;
	}
	public void setItemList(List<Items> itemList) {
		this.itemList = itemList;
	}
	public String getTeacher_id() {
		return teacher_id;
	}
	public void setTeacher_id(String teacher_id) {
		this.teacher_id = teacher_id;
	}
	public String getTeacher_name() {
		return teacher_name;
	}
	public void setTeacher_name(String teacher_name) {
		this.teacher_name = teacher_name;
	}
	public String getTeacher_img() {
		return teacher_img;
	}
	public void setTeacher_img(String teacher_img) {
		this.teacher_img = teacher_img;
	}
	public void setThumbnails(String thumbnails) {
		this.thumbnails = thumbnails;
	}
	public String getItemIdArr() {
		return itemIdArr;
	}
	public void setItemIdArr(String itemIdArr) {
		this.itemIdArr = itemIdArr;
	}
	public int getStarting_point() {
		return starting_point;
	}
	public void setStarting_point(int starting_point) {
		this.starting_point = starting_point;
	}
	public String getIframeUrl() {
		return iframeUrl;
	}
	public void setIframeUrl(String iframeUrl) {
		this.iframeUrl = iframeUrl;
	}
	
	
}
