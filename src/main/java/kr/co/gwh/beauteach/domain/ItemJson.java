package kr.co.gwh.beauteach.domain;

public class ItemJson {
	public String item_id;
	public String has_review;
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public String getHas_review() {
		return has_review;
	}
	public void setHas_review(String has_review) {
		this.has_review = has_review;
	}
	
	
}
