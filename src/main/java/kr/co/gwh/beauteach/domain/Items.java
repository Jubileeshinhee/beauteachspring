package kr.co.gwh.beauteach.domain;

public class Items {

	private int item_no;
	private int video_no;
	private int item_category;
	private int starting_point;
	private String item_id;
	private String url;
	private float viewer;
	private float rating;
	private int rater;
	private String thumbnail;
	private String price;
	private String product_name;
	private boolean has_review;
	private String review_url;
	
	
	
	public int getItem_no() {
		return item_no;
	}
	public void setItem_no(int item_no) {
		this.item_no = item_no;
	}
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public float getViewer() {
		return viewer;
	}
	public void setViewer(float viewer) {
		this.viewer = viewer;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public int getRater() {
		return rater;
	}
	public void setRater(int rater) {
		this.rater = rater;
	}
	public int getVideo_no() {
		return video_no;
	}
	public void setVideo_no(int video_no) {
		this.video_no = video_no;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public int getItem_category() {
		return item_category;
	}
	public void setItem_category(int item_category) {
		this.item_category = item_category;
	}
	public int getStarting_point() {
		return starting_point;
	}
	public void setStarting_point(int starting_point) {
		this.starting_point = starting_point;
	}
	public boolean isHas_review() {
		return has_review;
	}
	public void setHas_review(boolean has_review) {
		this.has_review = has_review;
	}
	public String getReview_url() {
		return review_url;
	}
	public void setReview_url(String review_url) {
		this.review_url = review_url;
	}
	@Override
	public String toString() {
		return "Items [item_no=" + item_no + ", video_no=" + video_no + ", item_category=" + item_category
				+ ", starting_point=" + starting_point + ", item_id=" + item_id + ", url=" + url + ", viewer=" + viewer
				+ ", rating=" + rating + ", rater=" + rater + ", thumbnail=" + thumbnail + ", price=" + price
				+ ", product_name=" + product_name + ", has_review=" + has_review + ", review_url=" + review_url + "]";
	}
	
	
}
