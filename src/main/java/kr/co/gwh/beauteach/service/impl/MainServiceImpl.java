package kr.co.gwh.beauteach.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import kr.co.gwh.beauteach.dao.MainDao;
import kr.co.gwh.beauteach.domain.Items;
import kr.co.gwh.beauteach.domain.Teacher;
import kr.co.gwh.beauteach.domain.User;
import kr.co.gwh.beauteach.domain.Videos;
import kr.co.gwh.beauteach.service.MainService;
import kr.co.gwh.beauteach.util.Common;
import kr.co.gwh.beauteach.util.JsoupParsing;

@Service
public class MainServiceImpl implements MainService{

	@Autowired
	MainDao mainDao;
	
	private static final Logger logger = LoggerFactory.getLogger(MainServiceImpl.class);
	
	
	@Override
	public int registVideos(Teacher teacher, Videos vo) {
		
		//이미 등록되어있는 뷰튜버 정보인지 확인
		Teacher existTeacher = mainDao.selectTeacherInfoById(teacher.getTeacher_id());
		
		if(existTeacher == null){
			//미등록 되어있으면 새로 등록해줌
			mainDao.insertTeacherInfo(teacher);
			logger.info("mainServiceImpl - registVideos - insertTeacherIfo insert seq->"+teacher.getTeacher_no());

			vo.setTeacher_no(teacher.getTeacher_no());
		}else {
			vo.setTeacher_no(existTeacher.getTeacher_no());
		}
		
		
		mainDao.insertVideoInfo(vo);
		
		logger.info("mainServiceImpl - registVideos - insertVideoInfo insert seq->"+vo.getVideo_no());
		
		for(int i=0;i<vo.getItemList().size();i++){
			vo.getItemList().get(i).setVideo_no(vo.getVideo_no());
			vo.getItemList().set(i, JsoupParsing.getReviewByIframeUrl(vo.getItemList().get(i)));
			mainDao.insertItemInfo(vo.getItemList().get(i));
		}
		
		//정상 등록 완료
		if(vo.getTeacher_no() > 0 && vo.getVideo_no() > 0){
			return vo.getVideo_no();
		}
		
		return 0;
	}
	
	@Override
	public Model getMainViewVideos(Model model, String category, String search_inp, int paging1, int paging2) {
		
		category = (category == null || category.equals("")) ? "0" : category;
		search_inp = (search_inp == null || search_inp.equals("")) ? "" : search_inp;
		//검색 조건으로 video list select
		List<Videos> videoList = mainDao.selectMainViewVideos(category, search_inp, paging1, paging2);
		String videoIdArr = "";
		
		for(int i=0;i<videoList.size();i++){
			//줄개행 줄바꿈 처리
			videoList.get(i).setDescription(videoList.get(i).getDescription().replaceAll("(\r\n|\n)", "<br/>"));
			//이미지 등록 안되어있으면 없는 이미지 처리
			videoList.get(i).setTeacher_img((videoList.get(i).getTeacher_img() == null ? "https://yt3.ggpht.com/-xYQ7zR87LhQ/AAAAAAAAAAI/AAAAAAAAAAA/TDSD7pAZwdU/s240-c-k-no-mo-rj-c0xffffff/photo.jpg" : videoList.get(i).getTeacher_img()));
			videoIdArr = (i == 0) ? (videoList.get(i).getVideo_id()) : (videoIdArr + "," + videoList.get(i).getVideo_id());
		}
		
		model.addAttribute("cate", category);
		model.addAttribute("search_inp", search_inp);
		model.addAttribute("videoList", videoList);
		model.addAttribute("videoIdArr", videoIdArr);
		
		return model;
	}
	
	@Override
	public Map<String, Object> getMainMoreVideos(Model model, String category, String search_inp, int paging1, int paging2) {
		
		category = (category == null || category.equals("")) ? "0" : category;
		search_inp = (search_inp == null || search_inp.equals("")) ? "" : search_inp;
		
		List<Videos> list = mainDao.selectMainViewVideos(category, search_inp, paging1, paging2);
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		
		return resultMap;
	}
	
	@Override
	public Model getBJVideoList(Model model, String bjNo, String searchInp, int paging1, int paging2) {
		
		String videoIdArr = "";
		String itemIdArr = "";
		List<Videos> videoList = mainDao.selectBjViewVideos(bjNo,searchInp, paging1, paging2);
		int cnt = 0;
		
		for(int i=0;i<videoList.size();i++){
			videoList.get(i).setDescription(videoList.get(i).getDescription().replaceAll("(\r\n|\n)", "<br/>"));
			videoList.get(i).setTeacher_img((videoList.get(i).getTeacher_img() == null ? "https://yt3.ggpht.com/-xYQ7zR87LhQ/AAAAAAAAAAI/AAAAAAAAAAA/TDSD7pAZwdU/s240-c-k-no-mo-rj-c0xffffff/photo.jpg" : videoList.get(i).getTeacher_img()));
			videoIdArr = (i == 0) ? (videoList.get(i).getVideo_id()) : (videoIdArr + "," + videoList.get(i).getVideo_id());
			
			//해당 동영상에 저장 된 items리스트 저장
			videoList.get(i).setItemList(mainDao.selectItemsInfoByVideoNo(videoList.get(i).getVideo_no(), 0, 0));
			itemIdArr = "";
			for(int j=0;j<videoList.get(i).getItemList().size();j++){
				itemIdArr= (j == 0) ? (videoList.get(i).getItemList().get(j).getItem_id()) : (itemIdArr + "," + videoList.get(i).getItemList().get(j).getItem_id());
			}
			videoList.get(i).setItemIdArr(itemIdArr);
			logger.info("getBJVideoList -> video_no :: "+videoList.get(i).getVideo_no()+" / " + "itemIdArr :: " + itemIdArr);
			
			if(!itemIdArr.equals("") && cnt == 0){
				cnt++;
				videoList.set(i, Common.AmazonCodeSnippets(videoList.get(i)));
			}
		}
		
		model.addAttribute("videoList", videoList);
		model.addAttribute("videoSize", videoList.size());
		model.addAttribute("videoIdArr", videoIdArr);
		return model;
	}
	
	@Override
	public Map<String, Object> getBJMoreVideos(Model model, String bjNo, String searchInp, int paging1, int paging2) {
		
		List<Videos> videoList = mainDao.selectBjViewVideos(bjNo,searchInp, paging1, paging2);
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", videoList);
		
		return resultMap;
	}
	
	@Override
	public Model getVideoDetailInfo(Model model, String videoNo) {
		
		Videos vo = mainDao.selectVideoInfoByNo(videoNo);
		
		vo.setItemList(mainDao.selectItemsInfoByVideoNo(Integer.parseInt(videoNo), 0, 1));
		vo.setDescription(vo.getDescription().replaceAll("(\r\n|\n)", "<br/>"));
		String itemIdArr = "";
		
		for(int j=0;j<vo.getItemList().size();j++){
			itemIdArr= (j == 0) ? (vo.getItemList().get(j).getItem_id()) : (itemIdArr + "," + vo.getItemList().get(j).getItem_id());
			vo.setTeacher_img((vo.getTeacher_img() == null ? "https://yt3.ggpht.com/-xYQ7zR87LhQ/AAAAAAAAAAI/AAAAAAAAAAA/TDSD7pAZwdU/s240-c-k-no-mo-rj-c0xffffff/photo.jpg" : vo.getTeacher_img()));
		}
		vo.setItemIdArr(itemIdArr);
		logger.info("getBJVideoList -> video_no :: "+vo.getVideo_no()+" / " + "itemIdArr :: " + itemIdArr);
		
		vo = Common.AmazonCodeSnippets(vo);
		
//		recomment list 가져오기
		String [] titleArray = vo.getTitle().split(" ");
		List<String> itemList = new ArrayList<String>();
		
		for(int i=0;i<vo.getItemList().size();i++){
			itemList.add(vo.getItemList().get(i).getProduct_name());
		}
		
		List<Videos> list = mainDao.selectVideoRecommend(titleArray, itemList, vo.getTeacher_no(), 0, 5);
		
		String videoIdArr = vo.getVideo_id();
		for(int i=0;i<list.size();i++){
			videoIdArr = (videoIdArr + "," + list.get(i).getVideo_id());
		}
		
		List<Integer> existCateList = mainDao.selectExistCateListByVideoNo(videoNo);
		
		List<Integer> cateList = new ArrayList<Integer>();
		
		Integer is_exist = 0;
		for(int i=0;i<7;i++) {
			is_exist = 0;
			for(int j=0;j< existCateList.size();j++){
				if(i == existCateList.get(j)){
					//category 있을 때
					is_exist = 1;
				}
			}
			cateList.add(is_exist);
		}
		model.addAttribute("cateList", cateList);
		model.addAttribute("vo", vo);
		model.addAttribute("list", list);
		model.addAttribute("videoIdArr", videoIdArr);
		model.addAttribute("listSize", vo.getItemList().size());
		
		return model;
	}
	
	@Override
	public Map<String, Object> getSubsMoreVideos(int paging1, int paging2, int video_no) {
		
		Videos vo = mainDao.selectVideoInfoByNo(String.valueOf(video_no));
		
		vo.setItemList(mainDao.selectItemsInfoByVideoNo(video_no, 0, 1));
		vo.setDescription(vo.getDescription().replaceAll("(\r\n|\n)", "<br/>"));
		String itemIdArr = "";
		
		for(int j=0;j<vo.getItemList().size();j++){
			itemIdArr= (j == 0) ? (vo.getItemList().get(j).getItem_id()) : (itemIdArr + "," + vo.getItemList().get(j).getItem_id());
		}
		vo.setItemIdArr(itemIdArr);
		vo = Common.AmazonCodeSnippets(vo);
		
//		recomment list 가져오기
		String [] titleArray = vo.getTitle().split(" ");
		List<String> itemList = new ArrayList<String>();
		
		for(int i=0;i<vo.getItemList().size();i++){
			itemList.add(vo.getItemList().get(i).getProduct_name());
		}
		
		List<Videos> list = mainDao.selectVideoRecommend(titleArray, itemList, vo.getTeacher_no(), paging1, paging2);
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		
		return resultMap;
	}
	
	@Override
	public Map<String, Object> getSubsItemsList(int video_no, int order_type, int cate) {
		
		Videos vo = new Videos();
		vo.setItemList(mainDao.selectItemsInfoByVideoNo(video_no, cate, order_type));
		
		String itemIdArr = "";
		
		for(int j=0;j<vo.getItemList().size();j++){
			itemIdArr= (j == 0) ? (vo.getItemList().get(j).getItem_id()) : (itemIdArr + "," + vo.getItemList().get(j).getItem_id());
		}
		vo.setItemIdArr(itemIdArr);
		vo = Common.AmazonCodeSnippets(vo);
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", vo.getItemList());
		return resultMap;
	}
	
	@Override
	public int loginChk(String id, String password, HttpServletRequest request) {
		User vo =  mainDao.loginChk(id, password);
		int result = 0;
		if(vo != null){
			result = 1;
			HttpSession session = request.getSession();
			session.setAttribute("user_id", vo.getUser_id());
			session.setAttribute("user_no", vo.getUser_no());
			session.setAttribute("user_name", vo.getName());
		}
		
		return result;
	}
	
	@Override
	public void updateExistedProductInformation() {
		List<String> itemIdList = mainDao.selectItemIdList();
		for(int i=0;i<itemIdList.size();i++){
			mainDao.updateExistedProductInformation(itemIdList.get(i), Common.getProductNameByProductId(itemIdList.get(i)));
		}
	}
	
	@Override
	public Model getAdminVideoList(Model model, int paging1, int paging2) {
		
		List<Videos> videoList = mainDao.selectMainViewVideos(null, null, paging1, paging2);
		int Listsize = mainDao.selectvideoTotalListSize();
		model.addAttribute("videoList", videoList);
		model.addAttribute("Listsize", Listsize);
		return model;
	}
	
	@Override
	public Model getAdminVideoDetail(Model model, String video_no) {
		
		Videos vo = mainDao.selectVideoInfoByNo(video_no);
		vo.setItemList(mainDao.selectItemsInfoByVideoNo(Integer.parseInt(video_no), 0, 0));
		
		model.addAttribute("vo", vo);
		model.addAttribute("itemCnt", vo.getItemList().size());
		return model;
	}
	
	@Override
	public int updateVideos(Teacher teacher, Videos vo) {
		//이미 등록되어있는 뷰튜버 정보인지 확인
		Teacher existTeacher = mainDao.selectTeacherInfoById(teacher.getTeacher_id());
		
		if(existTeacher == null){
			//미등록 되어있으면 새로 등록해줌
			mainDao.insertTeacherInfo(teacher);
			logger.info("mainServiceImpl - updateVideos - insertTeacherIfo insert seq->"+teacher.getTeacher_no());

			vo.setTeacher_no(teacher.getTeacher_no());
		}else {
			vo.setTeacher_no(existTeacher.getTeacher_no());
		}
		
		
		mainDao.updateVideoInfo(vo);
		mainDao.deleteItemsAllByVideoNo(vo.getVideo_no());
		
		for(int i=0;i<vo.getItemList().size();i++){
			vo.getItemList().get(i).setVideo_no(vo.getVideo_no());
			vo.getItemList().set(i, JsoupParsing.getReviewByIframeUrl(vo.getItemList().get(i)));
			mainDao.insertItemInfo(vo.getItemList().get(i));
		}
		
		//정상 등록 완료
		if(vo.getTeacher_no() > 0 && vo.getVideo_no() > 0){
			return vo.getVideo_no();
		}
		
		return 0;
	}
	
	@Override
	public Map<String, Object> getProductReviews(List<Map<String, Object>> itemList) {
		
		List<Items> itemResult = new ArrayList<Items>();
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		logger.info("listSize : "+itemList.size());
		
		for(int i=0;i<itemList.size();i++){
//			itemResult.add(i, JsoupParsing.getReviewByIframeUrl(itemList.get(i)));
		}
		
		resultMap.put("itemList", itemResult);
		
		return resultMap;
	}
	
	@Override
	public void updateExistingProductRating() {
		List<String> itemIdList = mainDao.selectItemIdList();
		
		Items vo = new Items();
		
		for(int i=0;i<itemIdList.size();i++){
			vo.setItem_id(itemIdList.get(i));
			mainDao.updateExistingProductRating(JsoupParsing.getReviewByIframeUrl(vo));
		}
	}
}
