package kr.co.gwh.beauteach.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import com.google.gson.JsonElement;

import kr.co.gwh.beauteach.domain.Items;
import kr.co.gwh.beauteach.domain.Teacher;
import kr.co.gwh.beauteach.domain.Videos;

public interface MainService {

	int registVideos(Teacher teacher, Videos vo);
	Model getMainViewVideos(Model model, String category, String string, int paging1, int paging2);
	Model getBJVideoList(Model model, String bjId, String searchInp, int paging1, int paging2);
	Map<String, Object> getMainMoreVideos(Model model, String category, String search_inp, int i, int j);
	Map<String, Object> getBJMoreVideos(Model model, String bjId, String searchInp, int paging1, int paging2);
	Model getVideoDetailInfo(Model model, String videoNo);
	Map<String, Object> getSubsMoreVideos(int paging1, int paging2, int video_no);
	Map<String, Object> getSubsItemsList(int video_no, int order_type, int cate);
	void updateExistedProductInformation();
	int loginChk(String id, String password, HttpServletRequest request);
	Model getAdminVideoList(Model model, int paging1, int paging2);
	Model getAdminVideoDetail(Model model, String video_no);
	int updateVideos(Teacher teacher, Videos vo);
	Map<String, Object> getProductReviews(List<Map<String, Object>> itemList);
	void updateExistingProductRating();

}
