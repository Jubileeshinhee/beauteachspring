package kr.co.gwh.beauteach.dao;

import java.util.List;

import kr.co.gwh.beauteach.domain.Items;
import kr.co.gwh.beauteach.domain.Teacher;
import kr.co.gwh.beauteach.domain.User;
import kr.co.gwh.beauteach.domain.Videos;

public interface MainDao {

	void insertTeacherInfo(Teacher teacher);
	void insertVideoInfo(Videos vo);
	void insertItemInfo(Items items);
	List<Videos> selectMainViewVideos(String category, String search_inp, int paging1, int paging2);
	Teacher selectTeacherInfoById(String teacher_id);
	List<Videos> selectBjViewVideos(String bjNo, String searchInp, int paging1, int paging2);
	List<Items> selectItemsInfoByVideoNo(int video_no, int cate, int order_type);
	Videos selectVideoInfoByNo(String videoNo);
	List<Integer> selectExistCateListByVideoNo(String videoNo);
	List<String> selectItemIdList();
	void updateExistedProductInformation(String productId, String productName);
	User loginChk(String id, String password);
	void updateVideoInfo(Videos vo);
	void deleteItemsAllByVideoNo(int video_no);
	int selectvideoTotalListSize();
	void updateExistingProductRating(Items vo);
	List<Videos> selectVideoRecommend(String[] titleArray, List<String> itemList, int teacher_no, int paging1, int paging2);

}
