package kr.co.gwh.beauteach.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.gwh.beauteach.dao.MainDao;
import kr.co.gwh.beauteach.domain.Items;
import kr.co.gwh.beauteach.domain.Teacher;
import kr.co.gwh.beauteach.domain.User;
import kr.co.gwh.beauteach.domain.Videos;

@Repository
public class MainDaoImpl implements MainDao{

	@Autowired 
	SqlSession sqlSession;
	String mapper = "MainMapper.";
	
	@Override
	public void insertTeacherInfo(Teacher teacher) {
		sqlSession.insert(mapper+"insertTeacherInfo",teacher);
	}
	
	@Override
	public void insertVideoInfo(Videos vo) {
		sqlSession.insert(mapper+"insertVideoInfo", vo);
	}
	
	@Override
	public void insertItemInfo(Items items) {
		sqlSession.insert(mapper+"insertItemInfo", items);
	}
	
	@Override
	public List<Videos> selectMainViewVideos(String category, String search_inp, int paging1, int paging2) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cate", (category == null || category.equals("0")) ? null : category);
		map.put("param", search_inp==null || "".equals(search_inp) ? null : "%"+search_inp+"%");
		map.put("paging1", paging1);
		map.put("paging2", paging2);
		return sqlSession.selectList(mapper+"selectMainViewVideos", map);
	}
	
	@Override
	public Teacher selectTeacherInfoById(String teacher_id) {
		return sqlSession.selectOne(mapper+"selectTeacherInfoById", teacher_id);
	}
	
	@Override
	public List<Videos> selectBjViewVideos(String bjNo, String searchInp, int paging1, int paging2) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("teacher_no", Integer.parseInt(bjNo));
		map.put("param", "%"+searchInp+"%");
		map.put("paging1", paging1);
		map.put("paging2", paging2);
		return sqlSession.selectList(mapper+"selectBjViewVideos", map);
	}
	
	@Override
	public List<Items> selectItemsInfoByVideoNo(int video_no, int cate, int order_type) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("video_no", video_no);
		map.put("cate", cate);
		map.put("order_type", order_type);
		return sqlSession.selectList(mapper+"selectItemsInfoByVideoNo", map);
	}
	
	@Override
	public Videos selectVideoInfoByNo(String videoNo) {
		return sqlSession.selectOne(mapper+"selectVideoInfoByNo", videoNo);
	}
	
	@Override
	public List<Integer> selectExistCateListByVideoNo(String videoNo) {
		return sqlSession.selectList(mapper+"selectExistCateListByVideoNo", videoNo);
	}
	
	@Override
	public List<String> selectItemIdList() {
		return sqlSession.selectList(mapper+"selectItemIdList");
	}
	
	@Override
	public void updateExistedProductInformation(String itemId, String productName) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("itemId", itemId);
		map.put("productName", productName);
		sqlSession.update(mapper+"updateExistedProductInformation", map);
	}

	@Override
	public User loginChk(String id, String password) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("password", password);
		return sqlSession.selectOne(mapper+"loginChk", map);
	}
	
	@Override
	public void updateVideoInfo(Videos vo) {
		sqlSession.update(mapper+"updateVideos", vo);
	}
	
	@Override
	public void deleteItemsAllByVideoNo(int video_no) {
		sqlSession.delete(mapper+"deleteItemsAllByVideoNo", video_no);
	}
	
	@Override
	public int selectvideoTotalListSize() {
		return sqlSession.selectOne(mapper+"selectvideoTotalListSize");
	}
	
	@Override
	public void updateExistingProductRating(Items vo) {
		sqlSession.update(mapper+"updateExistingProductRating", vo);
	}
	
	@Override
	public List<Videos> selectVideoRecommend(String[] titleArray, List<String> itemList, int teacher_no, int paging1,
			int paging2) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("titleArray", titleArray);
		map.put("itemList", itemList);
		map.put("teacher_no", teacher_no);
		map.put("paging1", paging1);
		map.put("paging2", paging2);
		
		return sqlSession.selectList(mapper+"selectVideoRecommend", map);
	}
}
