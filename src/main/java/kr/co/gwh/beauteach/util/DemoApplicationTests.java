package kr.co.gwh.beauteach.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class DemoApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void awsLink() throws IOException {
        final String url = "https://www.amazon.com/reviews/iframe?akid=AKIAI2I2FDKUFYXGAUTA&alinkCode=xm2&asin=B01G6KJSXC&atag=goodwillhunting&exp=2017-05-18T07%3A42%3A17Z&v=2&sig=7Q0hLeRzwf8xFJKvwrIpo%252FFdDWmPV1W0Eu29%252Fp7%252BH3s%253D";


        URLConnection connection = new URL(url).openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        connection.connect();

        BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            sb.append(line);
        }

//        final String filepath = "/Users/abel/Desktop/test.html";
//        {
//            FileOutputStream fos = new FileOutputStream(filepath);
//            fos.write("".getBytes());
//            fos.close();
//        }
//        {
////            FileInputStream fis = new FileInputStream(filepath);
//
//        }
//        {
//            FileOutputStream fos = new FileOutputStream(filepath);
//            fos.write(sb.toString().getBytes());
//            fos.close();
//        }

        Document document = Jsoup.parse(sb.toString());

        {
            Elements elements = document.select("span.asinReviewsSummary");
            Element element = elements.select("img").first();
            String alterText = element.attr("alt");
            int i = alterText.indexOf(" out of");
            alterText = alterText.substring(0, i);
            System.out.println(alterText);
        }
        {
            Elements elements = document.select("span.crAvgStars > a");
            String userCount = elements.first().text();
            System.out.println(userCount.replaceAll(" customer reviews", ""));
        }

        System.out.println("done");
    }

    @Test
    public void multipleRequest() throws IOException, InterruptedException {


        for (int i = 0; i < 500; i++) {

            awsLink();

            Thread.sleep(Math.round(10));//00 * new Random().nextFloat()));
        }

    }

}
