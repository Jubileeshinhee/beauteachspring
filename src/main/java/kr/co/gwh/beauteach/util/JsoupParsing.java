package kr.co.gwh.beauteach.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kr.co.gwh.beauteach.domain.Items;

public class JsoupParsing {
	
	private static final Logger logger = LoggerFactory.getLogger(JsoupParsing.class);
	
	public static Items getReviewByIframeUrl(Items items) {
		
		try {
			
			logger.info("item_id : "+items.getItem_id());
				
			final String url = "https://www.amazon.com/product-reviews/"+items.getItem_id();

	        URLConnection connection = new URL(url).openConnection();
	        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
	        connection.connect();

	        BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

	        StringBuilder sb = new StringBuilder();
	        String line;
	        while ((line = r.readLine()) != null) {
	            sb.append(line);
	        }
	        
	        logger.debug((sb.length() > 0) ? "recieved - " + sb.substring(0, 10) : "no response from amazon");
	        
	        Document document = Jsoup.parse(sb.toString());
	        {
	            items.setRating(Float.parseFloat(document.select("span.arp-rating-out-of-text").text().substring(0, 3)));
	        }
	        {
				items.setRater(Integer.parseInt(document.select("span.totalReviewCount").text().replaceAll(",", "")));
	        }
			
			logger.info("::getReviewByIframeUrl  --> result : "+items.getRating()+" / "+items.getRater());
			
			Thread.sleep(10);
		} catch (IOException e) {
			logger.error("IO Error - {}", e.getMessage());
		} catch (InterruptedException e) {
			logger.error("Sleep Exception - {}", e.getMessage());
		}
		
		return items;
	}

}
