package kr.co.gwh.beauteach.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import kr.co.gwh.beauteach.domain.Items;
import kr.co.gwh.beauteach.domain.Videos;

public class Common {
	
	private static final Logger logger = LoggerFactory.getLogger(Common.class);

	private static final String AWS_ACCESS_KEY_ID = "AKIAI2I2FDKUFYXGAUTA";

    /*
     * Your AWS Secret Key corresponding to the above ID, as taken from the AWS
     * Your Account page.
     */
    private static final String AWS_SECRET_KEY = "yWoGiwCUNgZVVCobOJHDRhQ+9TvD5kpWvrbXiFLa";

    /*
     * Use the end-point according to the region you are interested in.
     */
    private static final String ENDPOINT = "webservices.amazon.com";
	
    
    
    
	public static Videos AmazonCodeSnippets(Videos video){
		

        Map<String, String> params = new HashMap<String, String>();

        params.put("Service", "AWSECommerceService");
        params.put("Operation", "ItemLookup");
        params.put("AWSAccessKeyId", "AKIAI2I2FDKUFYXGAUTA");
        params.put("AssociateTag", "goodwillhunting");
        params.put("IdType", "ASIN");
        params.put("ResponseGroup", "Images,ItemAttributes,Reviews");
        
        
        if(video.getItemList().size() < 11){
        	params.put("ItemId", video.getItemIdArr());
        	//10개 이하
        	params.put("type", "1");
            video = AmazonCodeGetSnippets(video, params);
        } else {
        	
        	String [] arr = video.getItemIdArr().split(",");
        	String itemIdArr = "";

        	//10개 초과
        	params.put("type", "2");
        	
        	if(arr.length < 21){
        		for(int i=0;i<10;i++){
        			itemIdArr = ((i == 0) ? arr[i] : itemIdArr + ","+arr[i]);
        		}

        		params.put("ItemId", itemIdArr);
        		params.put("cnt", "0");
        		params.put("size", 10+"");

                video = AmazonCodeGetSnippets(video, params);
                
                for(int i = 10; i < arr.length; i++){
                	itemIdArr = ((i == 10) ? arr[i] : itemIdArr + ","+arr[i]);
                }
                params.put("ItemId", itemIdArr);
        		params.put("cnt", "1");
        		params.put("size", (arr.length-10)+"");

                video = AmazonCodeGetSnippets(video, params);
        	} else if(arr.length < 31){
        		for(int i=0;i<10;i++){
        			itemIdArr = ((i == 0) ? arr[i] : itemIdArr + ","+arr[i]);
        		}
        		
        		params.put("ItemId", itemIdArr);
        		params.put("cnt", "0");
        		params.put("size", 10+"");

                video = AmazonCodeGetSnippets(video, params);
                
                for(int i = 10; i < 20; i++){
                	itemIdArr = ((i == 10) ? arr[i] : itemIdArr + ","+arr[i]);
                }
                params.put("ItemId", itemIdArr);
        		params.put("cnt", "1");
        		params.put("size", 10+"");

                video = AmazonCodeGetSnippets(video, params);
                
                for(int i = 20; i < arr.length; i++){
                	itemIdArr = ((i == 20) ? arr[i] : itemIdArr + ","+arr[i]);
                }
                params.put("ItemId", itemIdArr);
        		params.put("cnt", "2");
        		params.put("size", (arr.length-20)+"");

                video = AmazonCodeGetSnippets(video, params);
        	}
        }
		return video;
	}
	
	public static Videos AmazonCodeGetSnippets(Videos video, Map<String, String> params) {
		
		String requestUrl = null;

		/*
         * Set up the signed requests helper.
         */
        SignedRequestsHelper helper;

        try {
            helper = SignedRequestsHelper.getInstance(ENDPOINT, AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
        } catch (Exception e) {
            e.printStackTrace();
            return video;
        }
		
		requestUrl = helper.sign(params);
		
		int type = Integer.parseInt(params.get("type"));

        logger.info("AmazonCodeSnippets :: Signed URL: \"" + requestUrl + "\"");
		
        try {
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(requestUrl);
			
			 // xpath 생성
			   XPath  xpath = XPathFactory.newInstance().newXPath();
			   
			   String expression = "";
			   
			   //10개 이하 일 때
			   if(type == 1) {
				   for(int i=0;i<video.getItemList().size();i++){
					   logger.info("DetailPageURL : "+video.getItemList().get(i).getUrl());
					   
					   expression = "//Item[ASIN='"+video.getItemList().get(i).getItem_id()+"']/MediumImage/URL";
					   video.getItemList().get(i).setThumbnail(xpath.compile(expression).evaluate(document));
					   logger.info("MediumImage : "+xpath.compile(expression).evaluate(document));
					   
					   if(video.getItemList().get(i).getThumbnail() == null || video.getItemList().get(i).getThumbnail().equals("")){
						   expression = "//Item[ASIN='"+video.getItemList().get(i).getItem_id()+"']/ImageSets/ImageSet[@Category='variant']/MediumImage/URL";
						   video.getItemList().get(i).setThumbnail(xpath.compile(expression).evaluate(document));
						   logger.info("2nd  MediumImage : "+xpath.compile(expression).evaluate(document));
					   } 
					   
					   if(video.getItemList().get(i).getThumbnail() == null || video.getItemList().get(i).getThumbnail().equals("")){
						   expression = "//Item[ASIN='"+video.getItemList().get(i).getItem_id()+"']/ImageSets/ImageSet[@Category='swatch']/MediumImage/URL";
						   video.getItemList().get(i).setThumbnail(xpath.compile(expression).evaluate(document));
						   logger.info("3  MediumImage : "+xpath.compile(expression).evaluate(document));
					   }
					   
					   expression = "//Item[ASIN='"+video.getItemList().get(i).getItem_id()+"']/ItemAttributes/ListPrice/FormattedPrice";
					   video.getItemList().get(i).setPrice(xpath.compile(expression).evaluate(document));
					   logger.info("Price : "+xpath.compile(expression).evaluate(document));
					   
					   logger.info("Product_name : "+video.getItemList().get(i).getProduct_name());
					   
					   expression = "//Item[ASIN='"+video.getItemList().get(i).getItem_id()+"']/CustomerReviews/HasReviews";
					   String result = xpath.compile(expression).evaluate(document);
					   if(result.equals("true")){
						   video.getItemList().get(i).setHas_review(true);
						   
						   expression = "//Item[ASIN='"+video.getItemList().get(i).getItem_id()+"']/CustomerReviews/IFrameURL";
						   
						   video.getItemList().get(i).setReview_url(xpath.compile(expression).evaluate(document));
						   /*video.getItemList().set(i, JsoupParsing.getReviewByIframeUrl(video.getItemList().get(i).getItem_id(), video.getItemList().get(i)));*/
					   }else{
						   video.getItemList().get(i).setHas_review(false);
					   }
				   }
				   
			   } else { // 10개 초과일 때

				   int cnt = Integer.parseInt(params.get("cnt"));
				   
				   for(int i=0;i<Integer.parseInt(params.get("size"));i++){
					   logger.info("DetailPageURL over 10 : "+video.getItemList().get((cnt*10 + i)).getUrl());
					   
					   expression = "//Item[ASIN='"+video.getItemList().get((cnt*10 + i)).getItem_id()+"']/MediumImage/URL";
					   video.getItemList().get((cnt*10 + i)).setThumbnail(xpath.compile(expression).evaluate(document));
					   logger.info("MediumImage : "+xpath.compile(expression).evaluate(document));
					   
					   if(video.getItemList().get((cnt*10 + i)).getThumbnail() == null || video.getItemList().get((cnt*10 + i)).getThumbnail().equals("")){
						   expression = "//Item[ASIN='"+video.getItemList().get((cnt*10 + i)).getItem_id()+"']/ImageSets/ImageSet[@Category='variant']/MediumImage/URL";
						   video.getItemList().get((cnt*10 + i)).setThumbnail(xpath.compile(expression).evaluate(document));
						   logger.info("2nd  MediumImage : "+xpath.compile(expression).evaluate(document));
					   }
					   
					   expression = "//Item[ASIN='"+video.getItemList().get((cnt*10 + i)).getItem_id()+"']/ItemAttributes/ListPrice/FormattedPrice";
					   video.getItemList().get((cnt*10 + i)).setPrice(xpath.compile(expression).evaluate(document));
					   logger.info("Price : "+xpath.compile(expression).evaluate(document));
					   
					   logger.info("Product_name : "+video.getItemList().get((cnt*10 + i)).getProduct_name());
					   
					   expression = "//Item[ASIN='"+video.getItemList().get((cnt*10 + i)).getItem_id()+"']/CustomerReviews/HasReviews";
					   String result = xpath.compile(expression).evaluate(document);
					   if(result.equals("true")){
						   video.getItemList().get((cnt*10 + i)).setHas_review(true);
						   expression = "//Item[ASIN='"+video.getItemList().get((cnt*10 + i)).getItem_id()+"']/CustomerReviews/IFrameURL";
						   video.getItemList().get((cnt*10 + i)).setReview_url(xpath.compile(expression).evaluate(document));
					   }else{
						   video.getItemList().get((cnt*10 + i)).setHas_review(false);
					   }
				   }
			   }

		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return video;
	}

	public static String getProductNameByProductId(String product_id){
		String result="";
		
		Map<String, String> params = new HashMap<String, String>();

        params.put("Service", "AWSECommerceService");
        params.put("Operation", "ItemLookup");
        params.put("AWSAccessKeyId", "AKIAI2I2FDKUFYXGAUTA");
        params.put("AssociateTag", "goodwillhunting");
        params.put("IdType", "ASIN");
        params.put("ResponseGroup", "Images,ItemAttributes,Reviews");
        params.put("ItemId", product_id);
        
        String requestUrl = null;

		/*
         * Set up the signed requests helper.
         */
        SignedRequestsHelper helper = null;

        try {
            helper = SignedRequestsHelper.getInstance(ENDPOINT, AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		requestUrl = helper.sign(params);
		
		try {
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(requestUrl);
			
			// xpath 생성
			XPath  xpath = XPathFactory.newInstance().newXPath();
			String expression = "";
			
			expression = "//Item[ASIN='"+product_id+"']/ItemAttributes/Title";
			result = xpath.compile(expression).evaluate(document);
			logger.info(product_id+"  result : "+result);
		   
		   
		   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
		
		return result;
	}
	
	public static void session_destroy(HttpServletRequest request){
		HttpSession session = request.getSession();
		//session.invalidate();
		session.setAttribute("user_id", "");
		session.setAttribute("user_name", "");
		session.setAttribute("user_no", "");

		System.out.println("Return : Session Destroy Success!");
	}
}
