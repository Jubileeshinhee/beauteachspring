package kr.co.gwh.beauteach.controller;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SessionInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		System.out.println("URI 주소 : "+request.getRequestURI());
		
		if(request.getRequestURI().contains("admin")){
		
			PrintWriter out = response.getWriter();
			HttpSession session = request.getSession();

			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			String user_id = session.getAttribute("user_id") == null ? "" : session.getAttribute("user_id").toString();
		
			if (user_id.equals("") || (user_id.equals("") && request.getRequestURI().equals("/admin"))) {
				out.println("<script>alert('로그인 정보가 없습니다. \\n로그인 페이지로 이동합니다.');location.href='/login';</script>");
				return false;
        	}
		}
		
        return super.preHandle(request, response, handler);
	}
}