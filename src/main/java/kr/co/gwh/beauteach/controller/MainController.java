package kr.co.gwh.beauteach.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.gwh.beauteach.domain.Items;
import kr.co.gwh.beauteach.domain.Teacher;
import kr.co.gwh.beauteach.domain.Videos;
import kr.co.gwh.beauteach.service.MainService;
import kr.co.gwh.beauteach.util.Common;

@Controller
public class MainController {
	
	@Autowired
	MainService mainService;
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String beauteachMainView(Model model, String category, String search_inp) {
		
		model = mainService.getMainViewVideos(model, category, search_inp, 0, 13);
		return "user/main_view";
	}
	
	@RequestMapping(value = "/login", method = { RequestMethod.GET, RequestMethod.POST })
	public String index(Model model, HttpServletRequest request) { // 로그인

		return "admin/login";
	}
	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminVideoList(Model model,Integer nowpage, Integer nowinpage) {
		nowpage = nowpage == null ? 1 : nowpage;
		nowinpage = nowinpage == null ? 1 : nowinpage;
		model = mainService.getAdminVideoList(model, 20*(nowpage-1), 20);
		model.addAttribute("nowpage", nowpage);
		model.addAttribute("nowinpage", nowinpage);
		return "admin/admin_video_list";
	}
	
	@RequestMapping(value = "/adminRegistVideo", method = RequestMethod.GET)
	public String adminRegistVideo(Model model) {
		return "admin/admin_regist_form";
	}
	
	@RequestMapping(value="/getMainMoreVideos.ajax", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getMainMoreVideos(Model model, String category, String search_inp, int getCnt){
		return mainService.getMainMoreVideos(model, category, search_inp, 13*getCnt, 10);
	}
	
	@RequestMapping(value="/registVideosSubmit", method={RequestMethod.GET, RequestMethod.POST})
	public String registVideosSubmit(Model model, HttpServletRequest request, String thumbnails, String teacher_id, Integer starting_point_m, Integer starting_point_s,
			String teacher_img, String teacher_name, String video_no, String video_id, String url, String title, String description){
		
		Videos vo = new Videos();
		Teacher teacher = new Teacher();
		List<Items> itemList = new ArrayList<Items>();
		Items items = new Items();
		
		starting_point_m = (starting_point_m == null || starting_point_m.equals(""))? 0 : starting_point_m;
		starting_point_s = (starting_point_s == null || starting_point_s.equals(""))? 0 : starting_point_s;
		
//		뷰튜버 저장 후 teacher_no 번호 반환
		if(teacher_id != null && !teacher_id.equals("")){
			teacher.setTeacher_id(teacher_id);
		}
		if(teacher_img != null && !teacher_img.equals("")){
			teacher.setTeacher_img(teacher_img);
		}
		if(teacher_name != null && !teacher_name.equals("")){
			teacher.setTeacher_name(teacher_name);
		}
		
		vo.setStarting_point((starting_point_m*60)+starting_point_s);
		
		if(video_id != null && !video_id.equals("")){
			vo.setVideo_id(video_id);
			vo.setUrl("https://www.youtube.com/watch?v="+video_id);
		}
		if(thumbnails!= null && !thumbnails.equals("")){
			vo.setThumbnail(thumbnails);
		}
		if(url != null && !url.equals("")){
			vo.setUrl(url);
		}
		if(title != null && !title.equals("")){
			vo.setTitle(title);
		}
		if(description != null && !description.equals("")){
			vo.setDescription(description);
		}
		String [] arr;
		
		if(request.getParameterValues("items_name") != null && !request.getParameterValues("items_name")[0].equals("")){
			for(int i=0;i<request.getParameterValues("items_name").length;i++){
				items = new Items();
				
				arr = request.getParameterValues("items_url")[i].split("\\/");
				
				if(request.getParameterValues("items_name")[i] != null && !request.getParameterValues("items_name")[i].equals("")){
					items.setProduct_name(request.getParameterValues("items_name")[i]);
				}
				if(request.getParameterValues("item_category")[i] != null && !request.getParameterValues("item_category")[i].equals("")){
					items.setItem_category(Integer.parseInt(request.getParameterValues("item_category")[i]));
				}
				
				items.setStarting_point( ((request.getParameterValues("items_starting_point_m")[i] == null 
						|| request.getParameterValues("items_starting_point_m")[i].equals("") ? 0 
								: Integer.parseInt(request.getParameterValues("items_starting_point_m")[i]))*60)
						+ (request.getParameterValues("items_starting_point_s")[i] == null 
						|| request.getParameterValues("items_starting_point_s")[i].equals("") ? 0 
								: Integer.parseInt(request.getParameterValues("items_starting_point_s")[i])));
				
				if(request.getParameterValues("items_url")[i] != null && !request.getParameterValues("items_url")[i].equals("")){
					items.setUrl(request.getParameterValues("items_url")[i]);
					items.setItem_id(arr[5]);
				}
				itemList.add(i, items);
			}
			vo.setItemList(itemList);
		}
		
		
		int result = mainService.registVideos(teacher, vo);
		String returnUrl = "redirect:/adminRegistVideo";
		
		if(result > 0){
			returnUrl = "redirect:/adminVideoDetail?video_no="+result;
			logger.info("video information insert success");
		}else{
			logger.info("video information insert fail");
		}
		
		return returnUrl;
	}
	
	@RequestMapping(value = "/bjDetailPage", method = {RequestMethod.GET, RequestMethod.POST})
	public String bjDetailPage(Model model, String bjNo,String searchInp, HttpServletRequest request) {

		model = mainService.getBJVideoList(model, bjNo, searchInp, 0, 13);
		return "user/bj_view";
	}
	
	@RequestMapping(value="/getBJMoreVideos.ajax", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getBJMoreVideos(Model model, String bjNo, String search_inp, int getCnt){
		return mainService.getBJMoreVideos(model, bjNo, search_inp, 13*getCnt, 10);
	}
	
	@RequestMapping(value = "/videoDetailPage", method = {RequestMethod.GET, RequestMethod.POST})
	public String videoDetailPage(Model model, String videoNo) {
		
		model = mainService.getVideoDetailInfo(model, videoNo);
		return "user/video_view";
	}
	
	@RequestMapping(value="/getSubsMoreVideos.ajax", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getSubsMoreVideos(Model model, int getCnt, int video_no){
		return mainService.getSubsMoreVideos(5*getCnt, 5, video_no);
	}
	
	@RequestMapping(value="/getSubsItemsList.ajax", method= RequestMethod.GET)
	public @ResponseBody Map<String, Object> getSubsItemsList(Model model, int order_type, int cate, int video_no) {
		return mainService.getSubsItemsList(video_no, order_type, cate);
	}
	
	@RequestMapping(value="/existedProductInformation", method=RequestMethod.GET)
	public void existedProductInformation(){
		mainService.updateExistedProductInformation();
	}
	
	@RequestMapping(value="/login_chk.ajax", method=RequestMethod.GET)
	public @ResponseBody int login_chk(String id, String password, HttpServletRequest request){
		
		return mainService.loginChk(id, password, request);
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model, HttpServletRequest request) {

		Common.session_destroy(request);

		return "redirect:/login";
	}
	
	@RequestMapping(value="/adminVideoDetail", method=RequestMethod.GET)
	public String adminVideoDetail(Model model, String video_no){
		model = mainService.getAdminVideoDetail(model, video_no);
		return "admin/admin_regist_detail";
	}
	
	@RequestMapping(value="/registVideosUpdate", method=RequestMethod.POST)
	public String registVideosUpdate(Model model, HttpServletRequest request, String thumbnails, String teacher_id, Integer starting_point_m, Integer starting_point_s,
			String teacher_img, String teacher_name, String video_no, String video_id, String title, String description,
			HttpServletResponse response){
		Videos vo = new Videos();
		Teacher teacher = new Teacher();
		List<Items> itemList = new ArrayList<Items>();
		Items items = new Items();
		
		starting_point_m = (starting_point_m == null || starting_point_m.equals(""))? 0 : starting_point_m;
		starting_point_s = (starting_point_s == null || starting_point_s.equals(""))? 0 : starting_point_s;
		
		if(teacher_id != null && !teacher_id.equals("")){
			teacher.setTeacher_id(teacher_id);
		}
		if(teacher_img != null && !teacher_img.equals("")){
			teacher.setTeacher_img(teacher_img);
		}
		if(teacher_name != null && !teacher_name.equals("")){
			teacher.setTeacher_name(teacher_name);
		}
		
		vo.setStarting_point((starting_point_m*60)+starting_point_s);
		if(video_id!= null && !video_id.equals("")){
			vo.setVideo_id(video_id);
		}
		if(video_no != null && !video_no.equals("")){
			vo.setVideo_no(Integer.parseInt(video_no));
		}
		if(thumbnails!= null && !thumbnails.equals("")){
			vo.setThumbnail(thumbnails);
		}
		if(title != null && !title.equals("")){
			vo.setTitle(title);
		}
		if(description != null && !description.equals("")){
			vo.setDescription(description);
		}
		
		String [] arr;
		
		if(request.getParameterValues("items_name") != null && !request.getParameterValues("items_name")[0].equals("")){
			for(int i=0;i<request.getParameterValues("items_name").length;i++){
				items = new Items();
				
				arr = request.getParameterValues("items_url")[i].split("\\/");
				
				if(request.getParameterValues("items_name")[i] != null && !request.getParameterValues("items_name")[i].equals("")){
					items.setProduct_name(request.getParameterValues("items_name")[i]);
				}
				if(request.getParameterValues("item_category")[i] != null && !request.getParameterValues("item_category")[i].equals("")){
					items.setItem_category(Integer.parseInt(request.getParameterValues("item_category")[i]));
				}
				
				items.setStarting_point( ((request.getParameterValues("items_starting_point_m")[i] == null 
						|| request.getParameterValues("items_starting_point_m")[i].equals("") ? 0 
								: Integer.parseInt(request.getParameterValues("items_starting_point_m")[i]))*60)
						+ (request.getParameterValues("items_starting_point_s")[i] == null 
						|| request.getParameterValues("items_starting_point_s")[i].equals("") ? 0 
								: Integer.parseInt(request.getParameterValues("items_starting_point_s")[i])));
				
				if(request.getParameterValues("items_url")[i] != null && !request.getParameterValues("items_url")[i].equals("")){
					items.setUrl(request.getParameterValues("items_url")[i]);
					items.setItem_id(arr[5]);
				}
				itemList.add(i, items);
			}
			vo.setItemList(itemList);
		}
		
		int result = mainService.updateVideos(teacher, vo);
		String returnUrl = "redirect:/adminRegistVideo";
		
		if(result > 0){
			returnUrl = "redirect:/adminVideoDetail?video_no="+result+"&result=1";
			logger.info("video information insert success");
			
		}else{
			logger.info("video information insert fail");
		}
		
		return returnUrl;
	}
	
	@RequestMapping(value="/getProductReviews.ajax" , method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> getProductReviews(@RequestBody List<Map<String, Object>> itemList,  HttpServletRequest request){
		
		return mainService.getProductReviews(itemList);
	}
	
	@RequestMapping(value="/updateProductRating", method=RequestMethod.GET)
	public void updateProductRating(){
		mainService.updateExistingProductRating();
	}
	
	
}
