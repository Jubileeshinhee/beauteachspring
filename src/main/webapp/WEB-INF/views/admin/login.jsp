<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ include file="/common/include/inc-top.jsp"%>
<link rel="stylesheet" type="text/css" href="/common/asset/css/bj_theme.css?ver=1">
</head>
<body class="Desktop_bj">
	<div class="Desktop_bj">
		<header class="header_wrap">
			<div class="">
				<!-- 큰 화면 -->
				<div class="search-bar">
					<a href="/">
						<img src="/common/asset/images/logo.png" class="logo">
					</a>
				</div>
				<!-- 큰 화면 -->
			</div>
		</header>
		<div class="regist_form">
			<form method="post" name="login_form" id="login_form" style="height: 300px;">
				<table id="login_table" class="login_table" style="margin: 0 auto;    padding-top: 114px;">
					<tbody>
						<tr>
							<td>Id</td>
							<td><input name="id" type="text" style="height: 25px;font-size: 18px;padding-left: 5px;" onkeypress="hitEnterKey(event,$('#submit_btn'))"></td>
							<td rowspan="2">
								<button type="button" id="submit_btn" style="height: 66px;width: 64px;">저장</button>
							</td>
						</tr>
						<tr>
							<td>Password</td>
							<td><input type="password" name="password" style="height: 25px;font-size: 18px;padding-left: 5px;" onkeypress="hitEnterKey(event,$('#submit_btn'))"></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<%@ include file="/common/include/inc-footer.jsp" %>
	</div>
</body>
</html>

<script type="text/javascript">
$("#submit_btn").click(function(){
	$.ajax({
		type: "GET",
		url: "/login_chk.ajax",
		data : "id="+$("input[name='id']").val()+"&password="+$("input[name=password]").val(), 
		cache: false,
		success: function (result) {
			if(result == 0){
				alert('아이디 / 비밀번호가 일치하지 않습니다');
				$("input[name=id]").focus();
			}else{
				location.href="/admin?nowpage=1";
			}
		},error : function (e){
			console.log(e);
  		}
	});
});

</script>