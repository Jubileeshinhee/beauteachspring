<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@ include file="/common/include/inc-top.jsp"%>

<link rel="stylesheet" type="text/css" href="/common/asset/css/bj_theme.css?ver=1">
</head>

<style>
<!--
.regist_btn {
	width: 100px;
    height: 30px;
    float: right;
    margin-top: 10px;
}

a {
	color: black;
	font-weight: bold; 
}
-->
</style>
<body class="Desktop_bj">
	<div class="Desktop_bj">
		<header class="header_wrap">
			<div class="">
				<!-- 큰 화면 -->
				<div class="search-bar">
					<a href="/">
						<img src="/common/asset/images/logo.png" class="logo">
					</a>
				</div>
				<!-- 큰 화면 -->
			</div>
		</header>
		<div class="regist_form">
			<div class="content-up" style="width: calc(100% - 15%);display: inline-block;margin-right: 15%;">
				<button id="regist_video" onclick="location.href='/adminRegistVideo'" class="regist_btn">비디오 등록</button>
				<button type="button" id="logout_btn" onclick="location.href='/logout'" class="regist_btn">로그아웃</button>
			</div>
			<table id="list_table" class="table table-bordered" style="margin: 0 auto;">
				<thead>
					<tr>
						<th>Video title</th>
						<th>Teacher name</th>
						<th>Upload date</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${videoList}" var="i">
						<tr>
							<td><a href="/adminVideoDetail?video_no=${i.video_no}">${i.title}</a></td>
							<td>${i.teacher_name}</td>
							<td>${fn:substring(i.date, 0, 10)}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="page_nav" align="center" id="pagging">
				<ul class="pagination">
				</ul>
			</div>
		</div>
		<%@ include file="/common/include/inc-footer.jsp" %>
	</div>
</body>
</html>
<script>
$(document).ready(function() {
	pagination(${nowpage},${nowinpage},20,${Listsize});
});

function pagination(pageNow,minpageNow,rowLimit,rowTotal){
	
	console.log(pageNow+" / "+minpageNow+" / "+rowLimit+" / "+rowTotal);
	
	// 현재 페이지 
	var nowpage = pageNow;
	// 
	var nowminpage = parseInt(minpageNow);
	// 로우 제한
	var limitrow = rowLimit == "-1" ? 9999 : rowLimit;
	// 페이징 넘버 제한
	var limitpage = 5;
	if (nowpage == "") {
		nowpage = 1;
	}
	if (nowminpage == "") {
		nowminpage = 1;
	}
	// 총 페이지 수 계싼
	var fvPageLength = Math.ceil(rowTotal / limitrow); // Max 페이지
	var fvTe = (Math.floor(nowminpage / limitpage) + 1) * limitpage;
	var fvPageShare = nowminpage * limitpage;
	if (fvPageShare > fvPageLength) {
		fvPageShare = fvPageLength;
	}
	
	var fvStr = "";
	
	fvStr += "<ul class='pagination'><li><span href='#' id='preview'>«</span></li>";
	
	// 페이지 숫자 생성
	for (var i = nowminpage; i <= fvTe; i++) {
		if (fvPageLength >= i) {
			if (nowpage == i) {
				fvStr += "<li id='li"+i+"' class='active'>";
			} else {
				fvStr += "<li id='li"+i+"'>";
			}
			fvStr += "<a id='p"+i+"' href='#'>";
			fvStr += i;
			fvStr += "</a>";
			fvStr += "</li>";
		}
	}
	fvStr += "<li><span href='#' id='nextview'>»</span></li></ul>";
	$("#pagging").get(0).innerHTML = fvStr;
	
	$("#preview").click(function() {
		var e = nowminpage - limitpage;
		var e2 = nowminpage - limitpage;
		if (e >= 1) {
			location.href="/admin?nowpage="+e+"&nowinpage="+e2;
		}
	});
	
	$("#nextview").click(function() {
		var e = nowminpage + limitpage;
		if (e <= fvPageLength) {
			location.href="/admin?nowpage="+e+"&nowinpage="+e;
		}
	});
	$(".pagination > li > a").bind("click",function(event){
		
		location.href="/admin?nowpage="+this.id.replace("p","");
	});
}

</script>