<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@ include file="/common/include/inc-top.jsp"%>
<style>
<!--
.regist_table input, .regist_table select, .regist_table textarea {
	width: 415px;
}
.regist_table  tr td:NTH-CHILD(2) {
	width: 415px;
}
.regist_btn {
	width: 100px;
    height: 30px;
    float: right;
    margin-top: 10px;
}
-->
</style>
<link rel="stylesheet" type="text/css" href="/common/asset/css/bj_theme.css?ver=1">
<!-- YOUTUBE API 로드 -->
<script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
<script src="/common/asset/js/custom.js?ver=4"></script>
</head>
<body class="Desktop_bj">
	<div class="Desktop_bj">
		<header class="header_wrap">
			<div class="">
				<!-- 큰 화면 -->
				<div class="search-bar">
					<a href="/">
						<img src="/common/asset/images/logo.png" class="logo">
					</a>
				</div>
				<!-- 큰 화면 -->
			</div>
		</header>
		<div class="regist_form">
			<div class="content-up" style="width: calc(100% - 15%);display: inline-block;margin-right: 15%;">
				<button id="regist_video" onclick="location.href='/admin?nowpage=1'" class="regist_btn">목차</button>
				<button type="button" id="logout_btn" onclick="location.href='/logout'" class="regist_btn">로그아웃</button>
			</div>
			<form action="/registVideosUpdate" method="post" name="regist_form" id="regist_form">
				<input type="hidden" value="" name="teacher_img">
				<input type="hidden" value="" name="thumbnails">
				<input type="hidden" value="${vo.video_no}" name="video_no">
				<input type="hidden" value="${itemCnt}" name="item_cnt" id="item_cnt">
				<table id="regist_table" class="regist_table" style="margin: 0 auto;width: 600px;">
					<tbody>
						<tr>
							<td>Video Id</td>
							<td colspan="3"><input name="video_id" type="text" value="${vo.video_id}" ></td>
						</tr>
						<tr>
							<td>Title</td>
							<td colspan="3"><input name="title" type="text" value="${vo.title}"></td>
						</tr>
						<tr>
							<td>Content</td>
							<td colspan="3">
							<textarea name="description" rows="5" style="padding: 0px; overflow-y: scroll;">${vo.description}</textarea>
							</td>
						</tr>
						<tr>
							<td>Video Start Time</td>
							<td style="width: 197px;">
								<fmt:parseNumber var="min" integerOnly="true" value="${(vo.starting_point / 60)}"/>
		                        <fmt:parseNumber var="sec" integerOnly="true" value="${(vo.starting_point mod 60)}"/>
								<input type="number" value="${min}" style="width: 160px;margin-right: 5px;" name="starting_point_m">min
							</td>
							<td></td>
							<td style="width: 197px;">
								<input type="number" value="${sec}" style="width: 160px;margin-right: 5px;" name="starting_point_s">sec
							</td>
						</tr>
						<tr>
							<td>BJ YOUTUBE Id</td>
							<td colspan="3"><input name="teacher_id" type="text" value="${vo.teacher_id}"></td>
						</tr>
						<tr>
							<td>BJ Name</td>
							<td colspan="3"><input name="teacher_name" type="text" value="${vo.teacher_name}"></td>
						</tr>
						<tr>
							<td colspan="5" style="text-align: center;">상품 목록
								<button type="button" onClick="add_items()">상품 추가</button>
								<button type="button" onClick="del_items()">상품 제거</button>
							</td>
						</tr>
						<c:forEach var="i" items="${vo.itemList}" varStatus="vs">
							<tr class='add_item_${(vs.index+1)}'>
								<td>Product Name</td>
								<td colspan='3'><input name='items_name' value="${i.product_name}" type="text"></td>
							</tr>
							<tr class='add_item_${(vs.index+1)}'>
								<td>Category</td>
								<td colspan='3'>
									<select name="item_category" style="width: 100%;">
										<option value="1" <c:if test="${i.item_category == 1}">selected</c:if>>Face</option>
										<option value="2" <c:if test="${i.item_category == 2}">selected</c:if>>Eyes</option>
										<option value="3" <c:if test="${i.item_category == 3}">selected</c:if>>Lips</option>
										<option value="4" <c:if test="${i.item_category == 4}">selected</c:if>>Tools</option>
									</select>
								</td>
							</tr>
							<tr class='add_item_${(vs.index+1)}'>
								<td>Product Url</td>
								<td colspan='3'><input name='items_url' type="text" value="${i.url}"></td>
							</tr>
							<tr class='add_item_${(vs.index+1)}'>
								<td>Item Appearance Time</td>
								<td style='width: 197px;'>
								<fmt:parseNumber var="min" integerOnly="true" value="${(i.starting_point / 60)}"/>
		                        <fmt:parseNumber var="sec" integerOnly="true" value="${(i.starting_point mod 60)}"/>
									<input type='number' style='width: 160px;margin-right: 5px;' value="${min}" name='items_starting_point_m'>min
								</td>
								<td></td>
								<td style='width: 197px;'>
									<input type='number' style='width: 160px;margin-right: 5px;' value="${sec}" name='items_starting_point_s'>sec
								</td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5" style="text-align: center;">
								<button type="button" onclick="getYoutubeInfo()">수정</button>
							</td>
						</tr>
					</tfoot>
				</table>
			</form>
		</div>
		<%@ include file="/common/include/inc-footer.jsp" %>
	</div>
</body>
</html>
<script>
$(document).ready(function() {
	
	if('${param.result}' == '1'){
		alert('수정이 완료되었습니다.');
	}
});
</script>