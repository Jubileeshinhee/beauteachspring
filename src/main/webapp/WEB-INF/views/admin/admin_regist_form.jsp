<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ include file="/common/include/inc-top.jsp"%>
<style>
<!--
.regist_table input, .regist_table select, .regist_table textarea {
	width: 415px;
}
.regist_table  tr td:NTH-CHILD(2) {
	width: 415px;
}
.regist_btn {
	width: 100px;
    height: 30px;
    float: right;
    margin-top: 10px;
}
-->
</style>
<link rel="stylesheet" type="text/css" href="/common/asset/css/bj_theme.css?ver=1">
<!-- YOUTUBE API 로드 -->
<script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
<script src="/common/asset/js/custom.js?ver=4"></script>
</head>
<body class="Desktop_bj">
	<div class="Desktop_bj">
		<header class="header_wrap">
			<div class="">
				<!-- 큰 화면 -->
				<div class="search-bar">
					<a href="/">
						<img src="/common/asset/images/logo.png" class="logo">
					</a>
				</div>
				<!-- 큰 화면 -->
			</div>
		</header>
		<div class="regist_form">
			<div class="content-up" style="width: calc(100% - 15%);display: inline-block;margin-right: 15%;">
				<button id="regist_video" onclick="location.href='/admin?nowpage=1'" class="regist_btn">목차</button>
				<button type="button" id="logout_btn" onclick="location.href='/logout'" class="regist_btn">로그아웃</button>
			</div>
			<form action="/registVideosSubmit" method="post" name="regist_form" id="regist_form">
				<input type="hidden" value="" name="teacher_img">
				<input type="hidden" value="" name="thumbnails"><input type="hidden" value="" name="url">
				<input type="hidden" value="" name="description">
				<input type="hidden" value="" name="teacher_id">
				<input type="hidden" value="" name="teacher_name">
				<input type="hidden" value="0" name="item_cnt" id="item_cnt">
				<table id="regist_table" class="regist_table" style="margin: 0 auto;width: 600px;">
					<tbody>
						<tr>
							<td>video Id</td>
							<td colspan="3"><input name="video_id"></td>
						</tr>
						<tr>
							<td>title</td>
							<td colspan="3"><input name="title"></td>
						</tr>
						<tr>
							<td>video start time</td>
							<td style="width: 197px;"><input type="number" style="width: 160px;margin-right: 5px;" name="starting_point_m">min</td>
							<td></td>
							<td style="width: 197px;"><input type="number" style="width: 160px;margin-right: 5px;" name="starting_point_s">sec</td>
						</tr>
						<tr>
							<td colspan="5" style="text-align: center;">상품 목록
								<button type="button" onClick="add_items()">상품 추가</button>
								<button type="button" onClick="del_items()">상품 제거</button>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5" style="text-align: center;">
								<button type="button" onclick="getYoutubeInfo()">저장</button>
							</td>
						</tr>
					</tfoot>
				</table>
			</form>
		</div>
		<%@ include file="/common/include/inc-footer.jsp" %>
	</div>
</body>
</html>
