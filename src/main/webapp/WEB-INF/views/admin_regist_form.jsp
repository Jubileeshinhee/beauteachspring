<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ include file="/common/include/inc-top.jsp"%>
<link rel="stylesheet" type="text/css" href="/common/asset/css/bj_theme.css">
<!-- YOUTUBE API 로드 -->
<script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
<script src="/common/asset/js/custom.js?ver=4"></script>
</head>
<body class="Desktop_bj">
	<div class="Desktop_bj">
		<header class="header_wrap">
			<div class="">
				<!-- 큰 화면 -->
				<div class="search-bar">
					<a href="/">
						<img src="/common/asset/images/logo.png" class="logo">
					</a>
				</div>
				<!-- 큰 화면 -->
			</div>
		</header>
		<div class="regist_form">
			<form action="/registVideosSubmit" method="post" name="regist_form" id="regist_form">
				<input type="hidden" value="" name="teacher_img">
				<input type="hidden" value="" name="thumbnails">
				<input type="hidden" value="0" name="item_cnt" id="item_cnt">
				<table id="regist_table" class="regist_table" style="margin: 0 auto;">
					<tbody>
						<tr>
							<td>url</td>
							<td><input name="url"></td>
						</tr>
						<tr>
							<td>video Id</td>
							<td><input name="video_id"></td>
						</tr>
						<tr>
							<td>제목</td>
							<td><input name="title"></td>
						</tr>
						<tr>
							<td>내용</td>
							<td><input name="description"></td>
						</tr>
						<tr>
							<td>카테고리</td>
							<td>
								<select name="category" style="width: 100%;">
									<option value="1">makeup</option>
									<option value="2">hair styling</option>
									<option value="3">nail art</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>비디오 시작 시간(초)</td>
							<td><input name="starting_point"></td>
						</tr>
						<tr>
							<td>bj youtube id</td>
							<td><input name="teacher_id"></td>
						</tr>
						<tr>
							<td>bj name</td>
							<td><input name="teacher_name"></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: center;">아이템 목록
								<button type="button" onClick="add_items()">상품 추가</button>
								<button type="button" onClick="del_items()">상품 제거</button>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="text-align: center;">
								<button type="button" onclick="getYoutubeInfo()">저장하기</button>
							</td>
						</tr>
					</tfoot>
				</table>
			</form>
		</div>
		<%@ include file="/common/include/inc-footer.jsp" %>
	</div>
</body>
</html>