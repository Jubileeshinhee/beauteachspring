<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="/common/asset/css/main_theme.css">
<%@ include file="/common/include/inc-top.jsp" %>
<meta property="fb:app_id" content="581981505333487" /> 

<meta property="og:title" content="${videoList[0].title }" />
<meta property="og:description" content="Just follow! Change your looks!" />
<meta property="og:site_name" content="BeauTeach" />
<meta property="og:url" content="http://beauteach.com" />
<meta property="og:type" content="video" />
<meta property="og:video" content="https://www.youtube.com/v/${videoList[0].video_id}" />

<meta property="og:image" content="${videoList[0].thumbnails}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="480" />
<meta property="og:image:height" content="360" />

</head>
<body>
<section class="Desktop_Main">
    <div class="header_wrap">
        <div class="container">
        <input type="hidden" name="getMoreTime" value="0">
        <input type="hidden" name="videoIdArr" id="videoIdArr" value="${videoIdArr}">
        <form name="frm_search" id="frm_search" method="get" action="/">
        	<input type="hidden" name="category" value="${param.category}">
        	<%@ include file="/common/include/inc-header.jsp" %>
            <div class="Makeup_category_line"></div>
            <div class="MakeUp-Category-box">
                <div class="scroll_div">
                    <ul class="depth02">
                        <li><button class="MakeUp-Category-General" style="width: 66px; margin-left:0px;" id="0"> <div class="text-font">ALL</div></button></li>
                        <li><button class="MakeUp-Category-General" style="width: 85px;" id="1"><div class="text-font">FACE</div></button></li>
                        <li><button class="MakeUp-Category-General" style="width: 68px" id="2"><div class="text-font">EYE</div></button></li>
                        <li><button class="MakeUp-Category-General" style="width: 85px" id="3"><div class="text-font">LIPS</div></button></li>
                        <li><button class="MakeUp-Category-General" style="width: 85px" id="5"><div class="text-font">HAIR</div></button></li>
                        <li><button class="MakeUp-Category-General" style="width: 85px" id="6"><div class="text-font">NAIL</div></button></li>
                    </ul>
                </div>
            </div>
        </form>
        </div>
       	<div class="main_img_div cycle-slideshow" data-cycle-prev="#prev" data-cycle-next="#next">
       		<img class="mySlides" src="/common/asset/images/main_head_bg01.png" style="display: none">
       		<img class="mySlides" src="/common/asset/images/main_head_bg02.png" style="display: none">
       	</div>
       	<div class="display_bottom">
			<div class="main_selected slide_div" id="prev">WELCOME</div>
			<div class="slide_div" id="next">HOW TO USE</div>
		</div>
       	<div id="jssor_1" class="main_img_div_mobile" style="position:relative;margin:0 auto;top:0px;left:0px;width:414px;height:224.375px;overflow:hidden;visibility:hidden;">
	        <!-- Loading Screen -->
	        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
	            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
	            <div style="position:absolute;display:block;background:url('/common/asset/images/loading.png') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
	        </div>
	        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:414px;height:224.375px;overflow:hidden;">
	            <div>
	                <img class="head_bg" data-u="image" src="/common/asset/images/mobile_head_bg00.png">
	       			<img class="head_text" src="/common/asset/images/mobile_head_text00.png">
	            </div>
	            <div>
	                <img class="head_bg" data-u="image" src="/common/asset/images/mobile_head_bg01.png">
	       			<img class="head_text" src="/common/asset/images/mobile_head_text01.png">
	            </div>
	            <div>
	                <img class="head_bg" data-u="image" src="/common/asset/images/mobile_head_bg02.png">
	       			<img class="head_text" src="/common/asset/images/mobile_head_text02.png">
	            </div>
	            <div>
	                <img class="head_bg" data-u="image" src="/common/asset/images/mobile_head_bg03.png">
	       			<img class="head_text" src="/common/asset/images/mobile_head_text03.png">
	            </div>
	        </div>
	        <!-- Bullet Navigator -->
	        <div data-u="navigator" class="jssorb05" style="bottom:14px;margin:0 auto;top:207px;left:-160px;right:0;" data-autocenter="1">
	            <!-- bullet navigator item prototype -->
	            <div data-u="prototype" style="width:6px;height:6px;"></div>
	        </div>
	    </div>
      	</div>
<c:choose>
	<c:when test="${videoList[0].title == null}">
		<div class="main_no_result">
			<div class="no_result_text">
				<div class="text_left">No results for <span style="font-weight: bold;">${search_inp}</span></div><div class="text_right">About 0 results</div>
			</div>
			<div class="text-area">Try search again using other keywords.</div>
		</div>
	</c:when>
	<c:otherwise>
		<div style="width: 100%;">
			<div class="main-big-view">
				<div class="video-space">
					<img style="cursor: pointer;" class="video-space" onclick="goVideoDetailPage(${videoList[0].video_no})" src="https://i.ytimg.com/vi/${videoList[0].video_id}/maxresdefault.jpg">
				</div>
				<div class="Headline-text"><a onclick="goVideoDetailPage(${videoList[0].video_no})">${videoList[0].title}</a></div>
				<div class="content-space">
					<a onclick="goBjDetailPage(${videoList[0].teacher_no})"><img src="${videoList[0].teacher_img}" class="beautuber-img"></a>
					<div class="content-up">
						<div class="from" style="margin-left: 12px;">from</div> <div class="beautuber"><a onclick="goBjDetailPage(${videoList[0].teacher_no})">${videoList[0].teacher_name}</a></div>
					</div>
					<div class="content-up">
						<div class="views" id="big_view_views"></div><div class="days-ago" id="big_view_ago"></div>
					</div>
				</div>
				<div class="content-right">
					<button class="showmore-box" onclick="goVideoDetailPage(${videoList[0].video_no})">
						<div class="text-font">Show More <img src="/common/asset/images/disclosure-indicator.svg" class="Disclosure-Indicator"></div>
					</button>
				</div>
			</div>
		</div>
		<div class="video-list-area">
			<div class="mt20 table_1" >
			<input type="hidden" id="is_done" name="is_done" value="0">
				<c:forEach var="list" items="${videoList}" varStatus="i">
					<c:if test="${!i.last}">
						<div class="inner_div">
							<table>
								<tr>
									<td rowspan="4">
										<a onclick="goVideoDetailPage('${videoList[(i.index+1)].video_no}')">
											<img class="video-line" 
											src="https://i.ytimg.com/vi/${videoList[(i.index+1)].video_id}/mqdefault.jpg">
										</a>
									</td>
									<td><a class="Headline-text" onclick="goVideoDetailPage('${videoList[(i.index+1)].video_no}')">${videoList[(i.index+1)].title}</a></td>
								</tr>
								<tr>
									<td>
										<div class="from">from</div>
										<div class="beautuber">
											<a onclick="goBjDetailPage(${videoList[(i.index+1)].teacher_no})">
												${videoList[(i.index+1)].teacher_name}
											</a>
										</div>
									</td>
								</tr>
								<tr>
									<td class="">
										<div class="views" id="video_list_views_${i.index}"></div>
										<div class="days-ago" id="video_list_ago_${i.index}"></div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="content-text">${videoList[(i.index+1)].description}</div>
									</td> 
								</tr>
							</table>
						</div>
					</c:if>
				</c:forEach>
			</div>
			<div class="showmore-line"></div>
			<a onclick="getMoreVideos()" class="showmore"> Show More </a>
		</div>
	</c:otherwise>
</c:choose>
	<%@ include file="/common/include/inc-footer.jsp" %>
</section>
</body>
</html>

<script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
<script src="/common/asset/js/main.js?Ver=2"></script>
<script src="/common/plugins/jssor_slider/jssor.slider-23.1.5.mini.js"></script>
<script src="/common/plugins/cycle_slider/jquery.cycle2.js"></script>

<script>

$(document).ready(function () {
	
	$("#${cate}").attr("class", "MakeUp-Category-Selected");
	
	$(".depth02 > li > button").bind("click",function(event){
		$(".depth02 .MakeUp-Category-Selected").attr("class", "MakeUp-Category-General");
		$(this).attr("class", "MakeUp-Category-Selected");
		
		$("input[name=category]").val(this.id);
		$("#frm_search").submit();
	});
	
});

function mainSearch(){
	$("input[name=category]").val('${cate}');
	$("#frm_search").submit();
}

</script>