<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Theme CSS -->

<link rel="stylesheet" type="text/css" href="/common/asset/css/bj_theme.css?ver=1">
<%@ include file="/common/include/inc-top.jsp" %>

<meta property="fb:app_id" content="581981505333487" /> 

<meta property="og:title" content="${videoList[0].title }" />
<meta property="og:description" content="Just follow! Change your looks!" />
<meta property="og:site_name" content="BeauTeach" />
<meta property="og:url" content="http://www.beauteach.com/bjDetailPage?bjNo=${param.bjNo}">
<meta property="og:type" content="video" />
<meta property="og:video" content="https://www.youtube.com/v/${videoList[0].video_id}" />

<meta property="og:image" content="${videoList[0].thumbnails}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="480" />
<meta property="og:image:height" content="360" />

<section class="Desktop_bj">
	<header class="header_wrap">
		<div class="container">
        <input type="hidden" name="getMoreTime" value="0">
        <input type="hidden" name="videoIdArr" id="videoIdArr" value="${videoIdArr}">
        <input type="hidden" name="bjNo" value="${param.bjNo}">
        <input type="hidden" name="teacher_id" value="${videoList[0].teacher_id}">
		<form name="frm_search" id="frm_search" method="post" action="/bjDetailPage">
			<%@ include file="/common/include/inc-header.jsp" %>
			<table class="bj_header_space">
				<tbody>
					<tr>
						<td class="left_array">
							<img class="profile_main" src="${videoList[0].teacher_img}">
						</td>
						<td class="right_array">
							<div class="beautuber"> ${videoList[0].teacher_name} </div>
							<div class="subscribed"><span id="sub_div"></span> &nbsp;<b class="middle_bar"></b> &nbsp;<span id="videos_div"></span></div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		</div>
	</header>
		<div class="big_view_space">
			<div class="video_space">
				<iframe id="ytplayer" allowFullScreen='allowFullScreen'
	                src="https://www.youtube.com/embed/${videoList[0].video_id}?autoplay=1&cc_load_policy=1&loop=1&modestbranding=1&playsinline=1&rel=0" >
	            </iframe>
            </div>
			<div class="video_information_area">
				<div class="title"><a onclick="goVideoDetailPage('${videoList[0].video_no}')">${videoList[0].title}</a></div>
				<div class="sub_views"><span id="big_view_views"></span><span id="big_view_ago"></span></div>
				<div class="content">${videoList[0].description}</div>
			</div>
            <div class="product_space" id="main_wrap">
            	<div id="book_list_area">
            		<div class="main_book_list">
            			<a class="arrow_left"><img src="/common/asset/images/ic_arrow_left.png" alt="이전 책"></a>
            			<a class="arrow_right"><img src="/common/asset/images/ic_arrow_right.png" alt="다음 책"></a>

            			<span class="bg bg_left"></span>
            			<span class="bg bg_right"></span>

            			<ul class="main_book_list selbook ">
            			<c:forEach var="list" items="${videoList[0].itemList}">
            				<li>
            					<a href="${list.url}" class="BtnPageView" alt="V99">
            						<table class="product_table">
                                    <tbody><tr>
                                    	<td><img class="product_img" src="${list.thumbnail}"></td>
                                    	<td>
											<div class="product_name">${list.product_name}</div>
											<div class="product_price">${list.price}</div>
										</td>
									</tr></tbody>
									</table>
								</a>
							</li>
            			</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="video-list-area">
			<div class="uploads_title">
				<input type="hidden" name="uploadNo" value="${videoSize}">
				Uploads <div class="uploads_title_number">${videoSize}</div>
			</div>
			<div class="showmore-line" id="uploads_showmore_line"></div>
			<div class="mt20 table_1" >
			<input type="hidden" id="is_done" name="is_done" value="0">
				<c:forEach var="list" items="${videoList}" varStatus="i">
					<c:if test="${!i.last}">
						<div class="inner_div">
							<table>
								<tr>
									<td rowspan="4">
										<a onclick="goVideoDetailPage('${videoList[(i.index+1)].video_no}')">
											<img class="video-line" 
											src="https://i.ytimg.com/vi/${videoList[(i.index+1)].video_id}/mqdefault.jpg">
										</a>
									</td>
									<td><a class="Headline-text" onclick="goVideoDetailPage('${videoList[(i.index+1)].video_no}')">${videoList[(i.index+1)].title}</a></td>
								</tr>
								<tr>
									<td><div class="from">from</div> <div class="beautuber"><a onclick="goBjDetailPage(${videoList[(i.index+1)].teacher_no})">${videoList[(i.index+1)].teacher_name}</a></div></td>
								</tr>
								<tr>
									<td >
										<div class="views" id="video_list_views_${i.index}"></div>
										<div class="days-ago" id="video_list_ago_${i.index}"></div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="content-text">${videoList[(i.index+1)].description}</div>
									</td> 
								</tr>
							</table>
						</div>
					</c:if>
				</c:forEach>
			</div>
			<div class="showmore-line"></div>
			<a onclick="getMoreVideos();" class="showmore">
				Show More
			</a>
		</div>
		<%@ include file="/common/include/inc-footer.jsp" %>
</section>
</html>
<script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
<script src="/common/asset/js/bj_detail.js?ver=2"></script>
    
