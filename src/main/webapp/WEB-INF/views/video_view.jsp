<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
  
  <!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="/common/asset/css/sub_theme.css?ver=1">
<link rel="stylesheet" type="text/css" href="/common/plugins/bootstrap-select/css/bootstrap-select.css?ver=1">
<link rel="stylesheet" type="text/css" href="/common/plugins/bootstrap/css/bootstrap.css?ver=1">
<%@ include file="/common/include/inc-top.jsp" %>
<meta property="fb:app_id" content="581981505333487" /> 

<meta property="og:title" content="${vo.title }" />
<meta property="og:description" content="Just follow! Change your looks!" />
<meta property="og:site_name" content="BeauTeach" />
<meta property="og:url" content="http://www.beauteach.com/videoDetailPage?videoNo=${param.videoNo}" />
<meta property="og:type" content="video" />
<meta property="og:video" content="https://www.youtube.com/v/${vo.video_id}" />

<meta property="og:image" content="${vo.thumbnails}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="480" />
<meta property="og:image:height" content="360" />

 <section class="Desktop_sub">
    <header class="header_wrap Desktop_Main">
        <div class="container">
        <input type="hidden" name="getMoreTime" value="0">
        <input type="hidden" name="videoIdArr" id="videoIdArr" value="${videoIdArr}">
        <input type="hidden" name="videoId" id="videoId" value="${vo.video_id}">
        <input type="hidden" name="video_no" id="video_no" value="${vo.video_no}">
        <form name="frm_search" id="frm_search" method="post" action="/bjDetailPage">
			<%@ include file="/common/include/inc-header.jsp" %>
		</form>
        </div>
    </header>
    <div>
        <div class="big_view_space">
            <div class="video_space">
            	<iframe id="player" class="img_size" type="text/html"
	                src="https://www.youtube.com/embed/${vo.video_id}?enablejsapi=1&start=${vo.starting_point}&cc_load_policy=1&loop=1&modestbranding=1&playsinline=1&rel=0" >
	            </iframe>
            </div>
        </div>
        <div class="video_information_area">
	        <div class="list_description">
	        	<div class="box_top">
		            <div class="contain_div">
		                <div class="Headline_text">${vo.title}</div>
		                <div class="content-space">
		                    <a onclick="goBjDetailPage(${vo.teacher_no})"><img src="${vo.teacher_img}" class="beautuber-img"></a>
		                    <div class="content-up">
		                        <div class="from" style="margin-left: 12px;">from</div> <div class="beautuber"><a onclick="goBjDetailPage(${vo.teacher_no})">${vo.teacher_name}</a></div>
		                    </div>
		                    <div class="content-up">
		                        <div id="big_view_views" class="views"></div><div id="big_view_ago" class="days-ago"></div>
		                    </div>
		                </div>
		                <div class="content-right">
		                    <a onclick="copy_trackback('http://www.beauteach.com/videoDetailPage?videoNo=${param.videoNo}')"><img src="/common/asset/images/ic_share_copy.png" class="url"></a>
		                    <a><img src="/common/asset/images/ic_share_facebook.png" id="shareBtn" class="facebook"></a>
		                </div>
		                <div class="content_right_mobile">
		                    <div class="table_line"></div>
		                    <a><img src="/common/asset/images/ic_share_facebook.png" id="shareBtn2" class="facebook"></a>
		                    <a onclick="copy_trackback('http://www.beauteach.com/videoDetailPage?videoNo=${param.videoNo}')"><img src="/common/asset/images/ic_share_copy.png" class="url"></a>
		                    <div class="table_line mt5"></div>
		                    <div class="menu_list_div">
		                        <div class="menu_1 tab selected" onclick="showTab(1)">Items</div>
		                        <div class="menu_3 tab" onclick="showTab(3)">Comment</div>
		                        <div class="menu_2 tab" onclick="showTab(2)">Description</div>
		                    </div>
		                </div>
		            </div>
		            <div class="subs_div tab_div" id="menu_2">
		                <div class="table_line"></div>
		                <div class="video_subs">${vo.description}</div>
		            </div>
		            <div class="item_list_div tab_div" id="menu_1">
		                <div class="table_line"></div>
		            <!-- start large -->
		                <div class="content_line_web">
		                    <div class="item_list_title">Item list</div> 
		                    <div class="list_size">
			                    <span id="item_list_size">${listSize}</span>
		                    </div>
		                    <div class="amazon_logo">
			                    <img src="/common/asset/images/amazon-logo.jpg">
		                    </div>
		                    <div class="item_select_div">
	    						<select class="selectpicker" name="order_type" onchange="sortByTags()">
									<option value="1" selected="selected">Popularity</option>
									<option value="2">Time</option>
								</select>
								
								<select class="selectpicker" name="cate" onchange="sortByTags()">
									<option value="0" selected="selected">All</option>
									<option value="1" <c:if test="${cateList[1] == 0}"> disabled="disabled"</c:if>>Face</option>
									<option value="2" <c:if test="${cateList[2] == 0}"> disabled="disabled"</c:if>>Eyes</option>
									<option value="3" <c:if test="${cateList[3] == 0}"> disabled="disabled"</c:if>>Lips</option>
									<option value="4" <c:if test="${cateList[4] == 0}"> disabled="disabled"</c:if>>Tools</option>
									<option value="5" <c:if test="${cateList[5] == 0}"> disabled="disabled"</c:if>>Hair</option>
									<option value="6" <c:if test="${cateList[6] == 0}"> disabled="disabled"</c:if>>Nail</option>
								</select>
							</div>
		                </div>
		                <table class="item_list_table">
		                <c:forEach var="i" items="${vo.itemList}">
		                    <tr>
		                        <td><a href="${i.url}" target="_blank">
		                            <table class="item_list_innertable">
		                                <tr>
		                                    <td><img class="product_img" src="${i.thumbnail}"></td>
		                                    <td style="padding-left: 10px;">
		                                    	<div>
		                                    		<div class="product_name">${i.product_name}</div>
		                                    		<div class="star_div">
	                                    				<fmt:parseNumber var="colored" integerOnly="true" value="${i.rating}"/>
				                                    	<fmt:parseNumber var="non_colored" integerOnly="true" value="${(5-i.rating)}"/>
				                                    	<c:set var="half_colored" value="${i.rating mod 1}"/>
				                                        <c:forEach var="col" begin="1" end="${colored}" varStatus="status">
				 											 <img class="product_starscore" src="/common/asset/images/ic_star_01.png">
				                                        </c:forEach>
				                                        <c:if test="${half_colored != 0}">
				                                        	<img class="product_starscore" src="/common/asset/images/ic_star_02.png">
				                                        </c:if>
				                                        <c:forEach var="col2" begin="1" end="${non_colored}" varStatus="status">
				 											<img class="product_starscore" src="/common/asset/images/ic_star_00.png">
				                                        </c:forEach>
				                                        <div class="product_score">${i.rater}</div>
		                                    		</div>
					                                <div class="product_price">
					                                    ${i.price}
					                                </div>
		                                    	</div>
		                                    </td>
		                                </tr>
		                            </table></a>
		                            <div class="seekTo_div">
		                            	<div class="seekTo_bar"></div>
		                            	<a onclick="seek(${i.starting_point})"><div class="fill-1">
		                            		<img src="/common/asset/images/fill-1.png">
		                            	</div>
		                            	<div class="seekTo_time">
		                            	<fmt:parseNumber var="min" integerOnly="true" value="${(i.starting_point / 60)}"/>
		                            	<fmt:parseNumber var="sec" integerOnly="true" value="${(i.starting_point mod 60)}"/>
		                            		<fmt:formatNumber  pattern="00" value="${min}"/>:<fmt:formatNumber pattern="00" value="${sec}"/>
		                            	</div>
		                            	</a>
		                            </div>
		                        </td>
		                    </tr>
		                    </c:forEach>
		                </table>
		         <!-- end large -->
		         <!-- start mobile -->
		                <div class="content_line_mobile">
		                    <div class="content_mobile_top">
			                    <div class="item_list_title">Item list</div> 
			                    <div class="list_size">
				                    <span id="item_list_size">${listSize}</span>
			                    </div>
			                    <div class="amazon_logo">
				                    <img src="/common/asset/images/amazon-logo.jpg">
			                    </div>
		                    </div>
		                    <div class="content_mobile_select">
	    						<select class="content_select selectpicker" name="order_type" onchange="sortByTags()">
									<option value="1" selected="selected">Popularity</option>
									<option value="2">Time</option>
								</select>
								
								<select class="content_select selectpicker" name="cate" onchange="sortByTags()">
									<option value="0" selected="selected">All</option>
									<option value="1" <c:if test="${cateList[1] == 0}"> disabled="disabled"</c:if>>Face</option>
									<option value="2" <c:if test="${cateList[2] == 0}"> disabled="disabled"</c:if>>Eyes</option>
									<option value="3" <c:if test="${cateList[3] == 0}"> disabled="disabled"</c:if>>Lips</option>
									<option value="4" <c:if test="${cateList[4] == 0}"> disabled="disabled"</c:if>>Tools</option>
									<option value="5" <c:if test="${cateList[5] == 0}"> disabled="disabled"</c:if>>Hair</option>
									<option value="6" <c:if test="${cateList[6] == 0}"> disabled="disabled"</c:if>>Nail</option>
								</select>
							</div>
		                </div>
		                <table class="item_list_table_mobile">
		                <c:forEach var="i" items="${vo.itemList}" varStatus="vs">
		                    <tr>
		                        <td>
		                        	<c:if test="${vs.index != 0}">
		                        		<div class="seekTo_bar"></div>
		                        	</c:if>
		                            <div class="seekTo_div">
		                            	<a onclick="seek(${i.starting_point})"><div class="fill-1">
		                            		<img src="/common/asset/images/fill-1.png">
		                            	</div>
		                            	<div class="seekTo_time">
		                            	<fmt:parseNumber var="min" integerOnly="true" value="${(i.starting_point / 60)}"/>
		                            	<fmt:parseNumber var="sec" integerOnly="true" value="${(i.starting_point mod 60)}"/>
		                            		<fmt:formatNumber  pattern="00" value="${min}"/>:<fmt:formatNumber pattern="00" value="${sec}"/>
		                            	</div>
		                            	</a>
		                            </div>
		                            <div class="seekTo_bar"></div>
		                        	<a href="${i.url}" target="_blank">
		                            	<table class="item_list_innertable">
			                                <tr>
			                                    <td><img class="product_img" src="${i.thumbnail}"></td>
			                                    <td style="padding-left: 10px;">
			                                    	<div>
			                                    		<div class="product_name">${i.product_name}</div>
			                                    		<div class="star_div">
		                                    				<fmt:parseNumber var="colored" integerOnly="true" value="${i.rating}"/>
					                                    	<fmt:parseNumber var="non_colored" integerOnly="true" value="${(5-i.rating)}"/>
					                                    	<c:set var="half_colored" value="${i.rating mod 1}"/>
					                                        <c:forEach var="col" begin="1" end="${colored}" varStatus="status">
					 											 <img class="product_starscore" src="/common/asset/images/ic_star_01.png">
					                                        </c:forEach>
					                                        <c:if test="${half_colored != 0}">
					                                        	<img class="product_starscore" src="/common/asset/images/ic_star_02.png">
					                                        </c:if>
					                                        <c:forEach var="col2" begin="1" end="${non_colored}" varStatus="status">
					 											<img class="product_starscore" src="/common/asset/images/ic_star_00.png">
					                                        </c:forEach>
					                                        <div class="product_score">${i.rater}</div>
			                                    		</div>
						                                <div class="product_price">
						                                    ${i.price}
						                                </div>
			                                    	</div>
			                                    </td>
			                                </tr>
			                            </table>
			                         </a>
		                        </td>
		                    </tr>
		                    </c:forEach>
		                </table>
		             <!-- end mobile -->
		            </div>
	            </div>
	            <div class="comment_div tab_div" id="menu_3">
	            	<div class="comment_write_area">
	            		<div style="display: inline-block;width: 100%;">
	            			<div class="item_list_title">Comments </div> <div class="list_size" id="comments_cnt">${listSize}</div>
	            		</div>
	            		<div class="comment_write_detail comment_write_login">
	            		<input type="hidden" name="videoId" id="videoId" value="${vo.video_id}">
	            			<img class="comment_write_img_area">
	            			<div style="display: inline-block;width: calc(100% - 59px);">
		            			<textarea class="commnet_write_input_area" id="textOriginal" placeholder="Add a new comment" onkeyup="chkIfNull(this)"></textarea>
	  						</div>
	            		</div>
	            		<div class="comment_write_nonLogin comment_write_detail" hidden>
	            			<img class="comment_write_img_area" src="https://yt3.ggpht.com/-xYQ7zR87LhQ/AAAAAAAAAAI/AAAAAAAAAAA/TDSD7pAZwdU/s50-c-k-no-mo-rj-c0xffffff/photo.jpg">
	            			<div style="display: inline-block;width: calc(100% - 59px);">
		            			<a href="#" id="login-link"><textarea class="commnet_write_input_area" placeholder="Add a new comment" disabled="disabled"></textarea></a>
	  						</div>
	            		</div>
	            		<div>
	            			<button class="addcomment_button_non add_comment_button" onclick="insertComment()" disabled="disabled">Add Comment</button>
	            		</div>
	            	</div>
	            	<div class="table_line"></div>
	                <div class="comment_area" >
	                <input type="hidden" name="nextPageToken" id="nextPageToken">
	                </div>
	            </div>
	        </div>
            <div class="video_line">
            <input type="hidden" name="is_done" id="is_done" value="0">
                <div class="title">Recommended</div>
                <div class="recommended_line"></div>
                <table class="recommended_table">
                	<c:forEach var="i" items="${list}" varStatus="vs">
						<tr>
							<td>
								<table class="inner_table">
									<tr>
										<td>
											<a onclick="goVideoDetailPage('${i.video_no}')">
												<img class="video_img" src="https://i.ytimg.com/vi/${i.video_id}/mqdefault.jpg">
											</a>
										</td>
										<td class="text_to_top">
											<div class="Headline"><a onclick="goVideoDetailPage('${i.video_no}')">${i.title}</a></div>
											<div class="content_line">
												<div class="from" style="margin-left: 5px;">from</div> 
												<div class="beautuber"><a onclick="goBjDetailPage(${i.teacher_no})">${i.teacher_name}</a></div>
												<div class="views" id="video_list_views_${vs.index}"></div>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
                	</c:forEach>
                </table>
                <div class="showmore_line"></div>
                <a onclick="getMoreVideos();" class="showmore">
                    Show More
                </a>
            </div>
        </div>
    </div>
    <%@ include file="/common/include/inc-footer.jsp" %>

</section>
</html>
<script src="http://www.youtube.com/player_api"></script>
<script src="/common/asset/js/auth.js?ver=1"></script>
<script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
<script src="/common/asset/js/custom.js?ver=1"></script>
<script src="/common/plugins/bootstrap/js/bootstrap.js"></script>
<script src="/common/plugins/bootstrap-select/js/bootstrap-select.js?ver=1"></script>
<script src="/common/asset/js/video_detail.js?ver=2"></script>
<script>

window.onload = function () {
	getVideoViews ();
	gapi.client.setApiKey('AIzaSyCzlsRfCnLVg38y42M0MBy6jBDILmRNv3Y');
    gapi.client.load('youtube', 'v3', function () {
    	CommentThread();
    });
	
	$('.selectpicker').selectpicker();
	
	$(".bootstrap-select.btn-group .dropdown-menu .disabled .text").css("color","#cecece");
	
};

</script>
