<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!-- 큰 화면 -->
	<div class="search-bar">
		<input type="hidden" name="search_chk_inp" id="search_chk_inp" value="0">
		<a href="/"><img src="/common/asset/images/logo.png" class="logo"></a>
		<div class="search_input_outdiv">
			<input class="search-box" name="search_inp" id="search_input" 
			value="${search_inp}" onkeyup="setSameSearchInp(this)"  onkeypress="hitEnterKey(event,$('#atm_search'))"
			placeholder="Search Videos, BJ, Cosmetics and more">
			<button class="search-button" type="button" onclick="mainSearch()" id="atm_search"><img src="/common/asset/images/ic_search.png"  class="ic_search"></button>
		</div>
	</div>
<!--큰 화면 -->
    
<!--작은화면 -->
	<div class="search_bar_mobile">
		<a href="/"><img src="/common/asset/images/logo.png"  class="logo"></a>
		<!-- <a href="#"><div class="mobile_ic_area"><img src="/common/asset/images/mobile_ic_menu.png" class="mobile_ic_menu"></div></a> -->
		<a href="#search_btn" onclick="searchbar();"><img src="/common/asset/images/mobile_ic_search.png"  class="mobile_ic_area"></a>
	</div>
<!-- 작은화면 -->
    
<!-- 작은화면 검색 -->
	<div class="search_bar_mobile2">
		<a type="button" href="#search_btn" id="atm_search_mobile" onclick="mainSearch()">
			<img src="/common/asset/images/mobile_ic_search_input.png" class="mobile_ic_search">
		</a>
		<input class="mobile_search_input" onkeypress="hitEnterKey(event,$('#atm_search_mobile'));"
		placeholder="Search Videos, BJ, Cosmetics and more" onblur="inputOutFocus()"
		value="${search_inp}" id="mobile_search_input" onfocus="chkInputOnFocus();"
		onkeyup="setSameSearchInp(this)">
		<a href="#close_mobile_search" onclick="close_mobile_searchbar()"><div class="mobile_search_close">CANCLE</div></a><div class="bar"></div>
		<a href="#delete_text" onclick="delete_text()"><img src="/common/asset/images/mobile_ic_delete.png" class="mobile_ic_delete"></a>
	</div>
<!-- 작은화면 검색 -->
