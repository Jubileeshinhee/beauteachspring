<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="utf-8">
<title>BeauTeach - Beauty&amp;Teach!</title>
<meta name="keywords" content="Beauty, Coach, Teach, Makeup, Cosmetic, Face, Skin, Lotion, Powder, Lipstick, Mascara, Moisture">
<meta name="author" content="GoodWillHunting, 굿윌헌팅 | gwh.co.kr">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="/common/asset/css/layout_common.css">
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-93739816-1', 'auto');
	ga('send', 'pageview');

</script>
