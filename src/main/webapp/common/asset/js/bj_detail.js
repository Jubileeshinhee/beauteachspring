$(document).ready(function() {
    //resizeContent();
    var cnt = 0;
    var loopcnt = $('.selbook').find('li').size()-1;
    $('.arrow_left').click(function(){
        $('.selbook').find('li').eq(0).before($('.selbook').find('>li:last-child'));
    });
    $('.arrow_right').click(function(){
        $('.selbook').append($('.selbook').find('>li:first-child'));
    });
    
});

function mainSearch(){
	location.href="/?category=0&search_inp="+$("input[name=search_inp]").val();
}

window.onload = function () {
	
	gapi.client.setApiKey('AIzaSyCzlsRfCnLVg38y42M0MBy6jBDILmRNv3Y');
    gapi.client.load('youtube', 'v3', function () {
    	getVideoViews ();
    	getBjsInfo();
    });
}

function getBjsInfo(){
	var channelId = $("input[name=teacher_id]").val();

	var subscribed = '';
	var videoCount = '';
	
    var request = gapi.client.youtube.channels.list({
        part: 'statistics',
        id: channelId
    });
    request.execute(function (response) {
        var srchItems = response.result.items;
        $.each(srchItems, function (index, item) {
        	subscribed = item.statistics.subscriberCount;
        	videoCount = item.statistics.videoCount;
        	$("#sub_div").html(toComma(subscribed)+" Subscribed");
        	$("#videos_div").html(toComma(videoCount)+" Videos");
        })
    })
}

//동영상 views, 날짜 수 가져오기
function getVideoViews (){
	var videoIdArrStr = $("#videoIdArr").val();
	
	var index = 0;
    var request = gapi.client.youtube.videos.list({
        part: 'snippet, statistics',
        id: videoIdArrStr
    });
    
    request.execute(function (response) {
        var srchStatistics = response.result.items;
        $.each(srchStatistics, function (index2, item) {
        	
        	var publishedAt = item.snippet.publishedAt.substring(0,10);
        	
        	if(index == 0) {
        		$("#big_view_views").html(toComma(item.statistics.viewCount)+" views");
        		$("#big_view_ago").html("&nbsp; | &nbsp;"+calDate(publishedAt));
        	} else {
        		$("#video_list_views_"+(index-1)).html(toComma(item.statistics.viewCount)+" views");
        		$("#video_list_ago_"+(index-1)).html("&nbsp; | &nbsp;"+calDate(publishedAt));
        	}
        	index ++ ;
        })
    })
}

$(window).resize(function() {
    //resizeContent();
});

function resizeContent() {
    var sliderWidth = $('.main_book_list ul').width();
    console.log(sliderWidth);
    $('.main_book_list ul').css({'width':(sliderWidth)+'px'});
    $('.main_book_list ul').css({'margin-left':-(sliderWidth/2)+'px'});
}

function getMoreVideos(){
	$("input[name=getMoreTime]").val(parseInt($("input[name=getMoreTime]").val()) + 1);
	
	var getCnt = $("input[name=getMoreTime]").val();
	var uploadNo = $("input[name=uploadNo]").val();
	var is_done = $("#is_done").val();
	if(is_done == 0){
		$.ajax({
			type: "GET",
			url: "/getBJMoreVideos.ajax",
			data : "getCnt="+getCnt+"&bjNo="+$("input[name=bjNo]").val()+"&search_inp="+$("input[name=search_inp]").val(), 
			cache: false,
			success: function (dataMap) {
				var result = dataMap['list'];
				
				var addStr = "";
				
				var index = $(".inner_div").length;
				
				var videoIdArr = $("#videoIdArr").val();
				
				if(result.length == 0) {
					$(".video-list-area .showmore").css("display", "none");
					$("#is_done").val(1);
				} else {
					$(result).each(function(idx, em){
						addStr += '<div class="inner_div">	'
							+'<table>	'
			                +'<tr>	'
			                    +'<td rowspan="4"><img class="video-line" src="https://i.ytimg.com/vi/'+em['video_id']+'/mqdefault.jpg"></td>	'
			                    +'<td><a class="Headline-text" onclick="goVideoDetailPage('+em['video_no']+')">'+em['title']+'</a></td>	'
			                +'</tr>	'
			                +'<tr>	'
			                    +'<td><div class="from">from</div><div class="beautuber"><a onclick="goBjDetailPage('+em['teacher_no']+')">'+em['teacher_name']+'</a></div></td>	'
			                +'</tr>	'
			                +'<tr>	'
			                    +'<td>	'
			                    	+'<div class="views" id="video_list_views_'+index+'"></div>	'	
			                    	+'<div class="days-ago" id="video_list_ago_'+index+'"></div>	'
			                    +'</td>	'
			                +'</tr>	'
			                +'<tr>	'
			                    +'<td>	'
			                    	+'<div class="content-text">'+em['description']+'</div>	'
			                    +'</td>	'
			                +'</tr>	'
			            +'</table>	'
				        +'</div>	';
				        
				        index++;
				        
				        if(videoIdArr != null && videoIdArr !=''){
				        	videoIdArr += ","+em['video_id'];
				        } else {
				        	videoIdArr += em['video_id'];
				        }
					})
					
					uploadNo = parseInt(uploadNo) + result.length;
					
					$(".table_1 ").append(addStr);
					$("#videoIdArr").val(videoIdArr);
					$("input[name='uploadNo']").val(uploadNo);
					$(".uploads_title_number").html(toComma(uploadNo));
					getVideoViews();
				}
			},error : function (e){
				console.log(e);
	  		}
		});
	}
}