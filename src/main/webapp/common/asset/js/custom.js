$(window).resize(function () {
	if($("#search_chk_inp").val() == '0'){
		var widths = window.innerWidth;
	    if (widths > 430) {
	        $(".search_bar_mobile").css("display", "none");
	        $(".search_bar_mobile2").css("display", "none");
	    } else {
	        close_mobile_searchbar();
	    }
	}
});

$(document).ready(function () {

	// Each time the user scrolls
	$(window).scroll(function() {
		// End of the document reached?
		if ((Math.round($(window).scrollTop()) + document.body.clientHeight) >= $(document).height()) {
			getMoreVideos();
		}
	});
	
});

function chkInputOnFocus(){
	$("#search_chk_inp").val(1);
}

function inputOutFocus(){
	$("#search_chk_inp").val(0);
}

function searchbar() {
	$("#search_chk_inp").val(1);
    $(".search_bar_mobile").css("display", "none");
    $(".search_bar_mobile2").css("display", "inline-block");
}

function delete_text() {
    $("#mobile_search_input").val("");
    $("#search_input").val("");
}

function close_mobile_searchbar() {
    $(".search_bar_mobile").css("display", "inline-block");
    $(".search_bar_mobile2").css("display", "none");
}

function getYoutubeInfo(){
	
	var index = ($("input[name=items_name]").index($("input[name=items_name]"))+1);
	
	var chk = true;
	
	if($("input[name=video_id]").val() == ''){
		alert("Please fill in the videoId.");
		$("input[name=video_id]").focus();
		chk = false;
		return false;
	} 
	
	if (chk && $("input[name=video_id]").val().includes("https://www.youtube.com")){
		alert("\'"+$("input[name=video_id]").val()+"\' is not in the format of Video Id.");
		$("input[name=video_id]").focus();
		chk = false;
		return false;
	}
	
	if (chk && $("input[name=video_id]").val().includes("watch?v=")){
		alert("\'"+$("input[name=video_id]").val()+"\' is not in the format of Video Id.");
		$("input[name=video_id]").focus();
		chk = false;
		return false;
	}
	
	if ($("input[name=title]").val() == ''){
		alert("Please fill in the title.");
		$("input[name=title]").focus();
		return false;
	}
	
	for(var i =0;i<index;i++){
		if ($("input[name=items_name]:eq("+i+")").val() == ''){
			alert("Please fill in the product name.");
			$("input[name=items_name]:eq("+i+")").focus();
			return false;
		}
		
		if ($("input[name=items_url]:eq("+i+")").val() == ''){
			alert("Please fill in the product url.");
			$("input[name=items_url]:eq("+i+")").focus();
			chk = false;
			return false;
		}
		
		if (chk && $("input[name=items_url]:eq("+i+")").val().split("/").length < 5){
			alert("\'"+$("input[name=items_url]:eq("+i+")").val()+"\' is not in the format of product url.");
			$("input[name=items_url]:eq("+i+")").focus();
			chk = false;
			return false;
		}
	}
	
	if($("#item_cnt").val() == 0){
		alert("Please add the Items.");
		return false;
	}
	
	gapi.client.setApiKey('AIzaSyCzlsRfCnLVg38y42M0MBy6jBDILmRNv3Y');
    gapi.client.load('youtube', 'v3', function () {
    	getVideosThumbnails();
    });
}

function setSameSearchInp(obj){
	$("#search_input").val(obj.value);
	$("#mobile_search_input").val(obj.value);
}

function getBeautuberImg(){
	var channelId = $("input[name=teacher_id]").val();

    var request = gapi.client.youtube.channels.list({
        part: 'snippet',
        id: channelId
    });
    request.execute(function (response) {
        var srchItems = response.result.items;
        $.each(srchItems, function (index, item) {
            $("input[name=teacher_img]").val(item.snippet.thumbnails.high.url);
            $("input[name=teacher_name]").val(item.snippet.title);
        })
        
        formSubmit();
    })
}

function getVideosThumbnails(){
	//비디오 ID Array String (id 배열을 "," 구분자로 splice)
	var videoIdArrStr = $("input[name=video_id]").val();

    var request = gapi.client.youtube.videos.list({
        part: 'snippet, statistics',
        id: videoIdArrStr
    });
    request.execute(function (response) {
        var srchItems = response.result.items;
        
        $.each(srchItems, function (index, item) {
            $("input[name=thumbnails]").val(item.snippet.thumbnails.high.url);
            $("input[name=description]").val(item.snippet.description);
            $("input[name=teacher_id]").val(item.snippet.channelId);
        })
        getBeautuberImg();
    })
}

function hitEnterKey(e,obj){
    if(e.keyCode == 13){
       $(obj).click();
    }
}

//동영상 날짜 계산
function calDate(start){
	var startd = new Date(start) ;
	var endd = new Date();
	var diff = endd.getTime() - startd.getTime();
	var returnStr = "";
	
	var days = Math.floor(diff / (1000*60*60*24) + 1);
	if(days>0 && days <= 7) {
		returnStr = "1 week ago";
	} else if(days > 7 && days <= 14) {
		returnStr = "2 weeks ago";
	} else if(days > 14 && days <= 21) {
		returnStr = "3 weeks ago";
	} else if(days > 21 && days <= 31) {
		returnStr = "1 month ago";
	} else if(Math.ceil(days/30) < 12) {
		returnStr = Math.ceil(days/30)+" months ago";
	} else if(Math.ceil(days/365) == 1) {
		returnStr = "1 year ago";
	} else {
		returnStr = Math.ceil(days/365)+" years ago";
	}
	
	return returnStr;
}

//3자리 콤마 찍기
function toComma(str) {
  str = String(str);
  return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function goVideoDetailPage(video_no){
	location.href="/videoDetailPage?videoNo="+video_no;
}

function goBjDetailPage(bjNo){
	location.href="/bjDetailPage?bjNo="+bjNo;
}

function formSubmit(){
	$("#regist_form").submit();
}
// admin 단 동영상 등록 폼, 상품 추가하기
function add_items() {
	var cnt = (parseInt($("#item_cnt").val()) + 1);
	$("#item_cnt").val(cnt);
	
	var str = "<tr class='add_item_"+cnt+"'>"
		+"	<td>product name</td>"
		+"	<td colspan='3'><input name='items_name'></td>"
		+"</tr><tr class='add_item_"+cnt+"'>"
		+"	<td>category</td> "
		+'	<td colspan="3"> <select name="item_category" style="width: 100%;">'
		+'			<option value="1">Face</option>'
		+'			<option value="2">Eyes</option>'
		+'			<option value="3">Lips</option>'
		+'			<option value="4">Tools</option>'
		+'		</select>'
		+'	</td>'
		+"</tr><tr class='add_item_"+cnt+"'>"
		+"	<td>product url</td>"
		+"	<td colspan='3'><input name='items_url'></td>"
		+"</tr><tr class='add_item_"+cnt+"'>"
		+"	<td>item appearance time</td>"
		+"	<td style='width: 197px;'><input type='number' style='width: 160px;margin-right: 5px;' name='items_starting_point_m'>min</td>"
		+'	<td></td>'
		+"	<td style='width: 197px;'><input type='number' style='width: 160px;margin-right: 5px;' name='items_starting_point_s'>sec</td>"
		+"</tr>";
	
    $("#regist_table tbody").append(str);
}

function del_items(){
	var cnt = $("#item_cnt").val();
	
	$(".add_item_"+cnt).remove();
	
	$("#item_cnt").val((parseInt(cnt)-1) <= 0 ? 0 : (parseInt(cnt)-1));
}

var ellipsisText = function (e, etc) {
    var wordArray = e.innerHTML.split(" ");
    while (e.scrollHeight > e.offsetHeight) {
        wordArray.pop();
        e.innerHTML = wordArray.join(" ") + (etc || "...");
    }
};