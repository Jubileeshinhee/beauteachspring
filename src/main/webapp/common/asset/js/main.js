window.onload = function () {
	getVideoViews ();
}

function mainSearch(){
	$("input[name=category]").val('${cate}');
	$("#frm_search").submit();
}

/* main page thumbnails slide */ 
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function currentDiv(n, obj) {
	showDivs(slideIndex = n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    if (n > (x.length -1) ) {slideIndex = 1} 
    if (n < 1) {slideIndex = (x.length -1)} ;
    for (i = 0; i < (x.length -1); i++) {
        $(".slide_div:eq("+i+")").removeClass("main_selected");
    }
    $(".slide_div:eq("+(slideIndex-1)+")").addClass("main_selected");
    
}

function currentMobileDiv(n, obj) {
	showMobileDivs(slideIndex = n);
	$(".mobile_slide_div").removeClass("main_selected");
	$(obj).addClass("main_selected");
}

function showMobileDivs(n) {
	
    var i;
    var x = document.getElementsByClassName("myMobileSlides");
    if (n > x.length) {slideIndex = 1} 
    if (n < 1) {slideIndex = x.length} ;
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[slideIndex-1].style.display = "inline-block";
    
}

jQuery(document).ready(function ($) {
	$(".main_img_div img").each(function() {
        var oImgWidth = $(this).width();
        var oImgHeight = $(this).height();
        
        $(this).css({
            'max-width':oImgWidth+'px',
            'max-height':oImgHeight+'px',
            'width':'100%',
            'height':'100%'
        });
    });
    
    var widths = window.innerWidth;
	
	if(widths < 1180){
		$(".main_img_div").css({
            'height':widths/2.62
		});
	}
	var jssor_1_SlideshowTransitions = [
		{$Duration:1200,$Opacity:2}
	];
	
	var jssor_1_options = {
			$AutoPlay: 1,
			$SlideshowOptions: {
				$Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
				$TransitionsOrder: 1
			},
			$ArrowNavigatorOptions: {
				$Class: $JssorArrowNavigator$
			},
			$BulletNavigatorOptions: {
				$Class: $JssorBulletNavigator$
			}
		};
	var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
	
	function ScaleSlider() {
		var widths = window.innerWidth;
		if(widths < 430){
			var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
	        if (refSize) {
		        refSize = Math.min(refSize, 414);
		        jssor_1_slider.$ScaleWidth(refSize);
		        
		        $(".main_img_div_mobile .head_text").css('top','48px');
		        $(".jssorb05").css('left','0');
		    }
		}
    }
	
    ScaleSlider();
    
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", ScaleSlider);
	
});

$( window ).resize(function() {
	var widths = window.innerWidth;
	
	if(widths < 1180){
		$(".main_img_div").css({
            'height':widths/2.62
		});
	}
	$(".main_img_div img").each(function() {
        var oImgWidth = $(this).width();
        var oImgHeight = $(this).height();
        $(this).css({
            'max-width':1180+'px',
            'max-height':450+'px',
            'width':'100%',
            'height':'100%'
        });
    });
});


//동영상 views, 날짜 수 가져오기
function getVideoViews (){
	gapi.client.setApiKey('AIzaSyCzlsRfCnLVg38y42M0MBy6jBDILmRNv3Y');
    gapi.client.load('youtube', 'v3', function () {
    	var videoIdArrStr = $("#videoIdArr").val();

    	var index = 0;
        var request = gapi.client.youtube.videos.list({
            part: 'snippet, statistics',
            id: videoIdArrStr
        });
        
        request.execute(function (response) {
            var srchStatistics = response.result.items;
            $.each(srchStatistics, function (index2, item) {
            	
            	var publishedAt = item.snippet.publishedAt.substring(0,10);
            	
            	if(index == 0) {
            		$("#big_view_views").html(toComma(item.statistics.viewCount)+" views");
            		$("#big_view_ago").html("&nbsp; | &nbsp;"+calDate(publishedAt));
            	} else {
            		$("#video_list_views_"+(index-1)).html(toComma(item.statistics.viewCount)+" views");
            		$("#video_list_ago_"+(index-1)).html("&nbsp; | &nbsp;"+calDate(publishedAt));
            	}
            	index ++ ;
            })
        })
    });
}

function getMoreVideos(){
	$("input[name=getMoreTime]").val(parseInt($("input[name=getMoreTime]").val()) + 1);
	
	var getCnt = $("input[name=getMoreTime]").val();
	var is_done = $("#is_done").val();
	if(is_done == 0){
		$.ajax({
			type: "GET",
			url: "/getMainMoreVideos.ajax",
			data : "getCnt="+getCnt+"&category="+$("input[name=category]").val()+"&search_inp="+$("input[name=search_inp]").val(), 
			cache: false,
			success: function (dataMap) {
				var result = dataMap['list'];
				
				var addStr = "";
				
				var index = $(".inner_div").length;
				
				var videoIdArr = $("#videoIdArr").val();
				
				if(result.length == 0) {
					$(".video-list-area .showmore").css("display", "none");
					$("#is_done").val(1);
				} else {
					$(result).each(function(idx, em){
						addStr += 
							'<div class="inner_div"> '
		                    +'<table> '
				                +'<tr> '
				                    +'<td rowspan="4"><img class="video-line" src="https://i.ytimg.com/vi/'+em['video_id']+'/mqdefault.jpg"></td> '
				                    +'<td><a class="Headline-text" onclick="goVideoDetailPage('+em['video_no']+')">'+em['title']+'</a></td> '
				                +'</tr> '
				                +'<tr> '
				                    +'<td><div class="from">from</div><div class="beautuber"><a onclick="goBjDetailPage('+em['teacher_no']+')">'+em['teacher_name']+'</a></div></td> '
				                +'</tr> '
				                +'<tr> '
				                    +'<td> '
				                    	+'<div class="views" id="video_list_views_'+index+'"></div> '
				                    	+'<div class="days-ago" id="video_list_ago_'+index+'"></div> '
				                    +'</td> '
				                +'</tr> '
				                +'<tr> '
				                    +'<td> '
				                    	+'<div class="content-text">'+em['description']+'</div> '
				                    +'</td> '
				                +'</tr> '
				            +'</table> '
				        +'</div>	';
				        
				        index++;
				        
				        if(videoIdArr != null && videoIdArr !=''){
				        	videoIdArr += ","+em['video_id'];
				        } else {
				        	videoIdArr += em['video_id'];
				        }
				        
				        
					})
					
					$(addStr).appendTo(".table_1 ");
					$("#videoIdArr").val(videoIdArr);
					getVideoViews();
				}
			},error : function (e){
				console.log(e);
	  		}
		});
	}
}
