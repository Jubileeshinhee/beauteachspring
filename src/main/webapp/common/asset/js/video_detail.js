/* ifram script */
var player;

function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		events: {
			'onReady': onPlayerReady
		}
	});
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
	event.target.playVideo();
}

function seek(sec){
	 if(player){
        player.seekTo(parseInt(sec), true);
    }
}

function showTab(val) { 
        
    $(".tab_div").css("display", "none");
    $(".tab").removeClass("selected");
    
    for(var i = 1; i < 4; i++) {
        if (i != val) { 
            $(".menu_"+val).addClass("selected");
            $("#menu_"+val).css("display", "inline-block");
        }
    }
    
    //comment 일 때 recommend부분 숨기기
    if(val == 3) {
    	$(".video_line").hide();
    }else {
    	$(".video_line").show();
    }
}

$(document).ready(function () {
	
	// Each time the user scrolls
	$(window).scroll(function() {
		// End of the document reached?
		if ((Math.round($(window).scrollTop()) + document.body.clientHeight) >= $(document).height()) {
			gapi.client.setApiKey('AIzaSyCzlsRfCnLVg38y42M0MBy6jBDILmRNv3Y');
		    gapi.client.load('youtube', 'v3', function () {
		    	CommentThread();
		    });
		}
	});
	
});

function chkIfNull(obj){
	if(obj.value != '' && obj.value != null){
		$(".add_comment_button").removeClass("addcomment_button_non");
		$(".add_comment_button").addClass("addcomment_button_active");
	}else {
		$(".add_comment_button").removeClass("addcomment_button_active");
		$(".add_comment_button").addClass("addcomment_button_non");
	}
	$(".addcomment_button_active").removeAttr('disabled');
	$(".addcomment_button_non").attr("disabled","disabled");
}

function CommentThread(){
	
    var request = gapi.client.youtube.commentThreads.list({
        part: 'snippet',
        videoId: $("#videoId").val(),
        maxResults: 10,
        textFormat:"html",
        pageToken:$("#nextPageToken").val(),
        order:'time'
    });
    
    request.execute(function (response) {
        var commentItems = response.result.items;
        var innerStr = '';
        var textDisplay = '';
        var authorDisplayName = '';
        var authorProfileImageUrl = '';
        var authorChannelUrl = '';
        var authorChannelId = '';
        var publishedAt = '';
        var tmp = $("#nextPageToken").val();
        $("#nextPageToken").val(response.result.nextPageToken);
        
        $.each(commentItems, function (index, item2) {
        	
        	textDisplay = item2.snippet.topLevelComment.snippet.textDisplay;
        	textDisplay = textDisplay.replace("\\\\n", "<br/>");
        	authorDisplayName = item2.snippet.topLevelComment.snippet.authorDisplayName;
        	authorProfileImageUrl = item2.snippet.topLevelComment.snippet.authorProfileImageUrl;
        	authorProfileImageUrl = authorProfileImageUrl.replace('s28', 's48'); 
        	authorChannelUrl = item2.snippet.topLevelComment.snippet.authorChannelUrl;
        	authorChannelId = item2.snippet.topLevelComment.snippet.authorChannelId.value;
        	publishedAt = item2.snippet.topLevelComment.snippet.publishedAt;
        	
        	if((tmp != null && tmp != '') && index == 0){
        		innerStr += '<div class="comment_middle_bar"></div>';
        	}
        	
            innerStr += '<table class="comment-renderer-content">	'
            	+ '<tr>	'
            	+ '<td rowspan="2" valign="top">	'
            	+ '<a href="'+authorChannelUrl+'"><img class="yt-thumb-square" src="'+authorProfileImageUrl+'" onload=";window.__ytRIL && __ytRIL(this)"></a>	'
            	+ '</td>	'
            	+ '<td>	'
            	+ '<div class="comment-renderer-header">	'
            	+ '<a href="'+authorChannelUrl+'">'+authorDisplayName+'</a>	'
            	+ '<span class="comment-renderer-time" tabindex="0">'+calDate(publishedAt)+'</span>	'
            	+ '</div>	'
            	+ '</td>	'
            	+ '</tr>	<tr>	'
            	+ '<td>	<div class="comment-renderer-text" tabindex="0" role="article">	'
            	+ '<div class="comment-renderer-text-content">'+textDisplay+'</div>	'
            	+ '</div>	'
            	+ '</td>	'
            	+ '</tr>	'
            	+ '</table>	';
            
            if(index != (commentItems.length-1)){
            	innerStr += '<div class="comment_middle_bar"></div>';
            }
        });
        
        $(".comment_area").append(innerStr);
    });
}

//동영상 views, 날짜 수 가져오기
function getVideoViews (){
	gapi.client.setApiKey('AIzaSyCzlsRfCnLVg38y42M0MBy6jBDILmRNv3Y');
    gapi.client.load('youtube', 'v3', function () {
	
		var videoIdArrStr = $("#videoIdArr").val();
	
		var index = 0;
		var request = gapi.client.youtube.videos.list({
			part: 'snippet, statistics',
			id: videoIdArrStr
		});
	  
		request.execute(function (response) {
			var srchStatistics = response.result.items;
			$.each(srchStatistics, function (index2, item) {
	      	
				var publishedAt = item.snippet.publishedAt.substring(0,10);
	      	
				if(index == 0) {
					$("#big_view_views").html(toComma(item.statistics.viewCount)+" views");
					$("#big_view_ago").html("&nbsp; | &nbsp;"+calDate(publishedAt));
					$("#comments_cnt").html(toComma(item.statistics.commentCount));
				} else {
					$("#video_list_views_"+(index-1)).html(toComma(item.statistics.viewCount)+" views");
				}
				index ++ ;
			});
		});
  
    });
}

function mainSearch(){
	location.href="/?category=0&search_inp="+$("input[name=search_inp]").val();
}

function getMoreVideos(){
	$("input[name=getMoreTime]").val(parseInt($("input[name=getMoreTime]").val()) + 1);
	
	var getCnt = $("input[name=getMoreTime]").val();
	var is_done = $("#is_done").val();
	
	if(is_done == 0){
		$.ajax({
			type: "GET",
			url: "/getSubsMoreVideos.ajax",
			data : "getCnt="+getCnt+"&video_no="+$("#video_no").val(), 
			catch: false,
			success: function (dataMap) {
				var result = dataMap['list'];
				
				var addStr = "";
				
				var index = $(".inner_table").length;
				
				var videoIdArr = $("#videoIdArr").val();
				
				if(result.length == 0) {
					$(".video_information_area .showmore").css("display", "none");
					$("#is_done").val(1);
				} else {
					$(result).each(function(idx, em){
						addStr += '<tr><td>'
		                    +'<table class="inner_table">'
				                +'<tr>'
				                    +'<td><a onclick="goVideoDetailPage('+em['video_no']+')"><img class="video_img" src="https://i.ytimg.com/vi/'+em['video_id']+'/mqdefault.jpg"></a></td>'
				                    +'<td class="text_to_top">'
				                    +'	<div class="Headline"><a onclick="goVideoDetailPage('+em['video_no']+')">'+em['title']+'</a></div>'
				                    +'	<div class="content_line">'
				                    +'		<div class="from" style="margin-left: 5px;">from</div> <div class="beautuber"><a onclick="goBjDetailPage('+em['teacher_no']+')">'+em['teacher_name']+'</a></div>'
				                    +'		<div class="views" id="video_list_views_'+index+'"></div>	'
				                    +'	</div>'
				                    +'</td>'
				                +'</tr>'
				            +'</table>'
				        +'</td></tr>';
				        
				        index++;
				        
				        if(videoIdArr != null && videoIdArr !=''){
				        	videoIdArr += ","+em['video_id'];
				        } else {
				        	videoIdArr += em['video_id'];
				        }
					});
					
					$(".recommended_table ").append(addStr);
					$("#videoIdArr").val(videoIdArr);
					getVideoViews();
				}
			},error : function (e){
				console.log(e);
	  		}
		});
	}
}

function copy_trackback(trb) {

    var IE=(document.all)?true:false;

    if (IE) {
        if(confirm("이 글의 트랙백 주소를 클립보드에 복사하시겠습니까?")){
        	window.clipboardData.setData("Text", trb);
        }
    } else {
        temp = prompt("Copy and use the link below",trb);
    }
	 
}

window.fbAsyncInit = function() {
	FB.init({
		appId      : '581981505333487',
		xfbml      : true,
		version    : 'v2.8'
	});
	FB.AppEvents.logPageView();
};

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function sortByTags() {
	var order_type = 0;
	var cate = 0;
	var video_no = $("#video_no").val();
	var widths = window.innerWidth;
	
	if(widths > 795){
		cate = $("select[name=cate]:eq(0)").val();
		order_type = $("select[name=order_type]:eq(0)").val();
	}else {
		cate = $("select[name=cate]:eq(1)").val();
		order_type = $("select[name=order_type]:eq(1)").val();
	}
	
	$.ajax({
		type: "GET",
		url: "/getSubsItemsList.ajax",
		data : "order_type="+order_type+"&cate="+cate+"&video_no="+video_no, 
		cache: false,
		success: function (dataMap) {
			var result = dataMap['list'];
			var colored = 0;
			var non_colored = 0;
			var half_colored = 0;
			var addStr = '';
			var addMobileStr = '';
			var min = '';
			var sec = '';
			
			$(".item_list_table").empty();
			$(".item_list_table_mobile").empty();
			if(result.length == 0) {
				$("#item_list_size").html('0');
			} else {
				
				$("#item_list_size").html(result.length);
				$(result).each(function(idx, em){
					
					colored = 0;
					non_colored = 0;
					half_colored = 0;
					
					colored = parseInt(em['rating']);
					non_colored = parseInt(5-em['rating']);
					half_colored = (em['rating'] % 1);
					
					min = parseInt(em['starting_point'] / 60) < 10 ? "0"+parseInt(em['starting_point'] / 60) : parseInt(em['starting_point'] / 60)+"";
					sec = (em['starting_point'] % 60) < 10 ? "0"+(em['starting_point'] % 60) : (em['starting_point'] % 60)+"";
					
					addStr += '<tr><td>	'
	                    +'<a href="'+em['url']+'" target="_blank">	'
			                +'<table class="item_list_innertable">	'
			                    +'<tr>	'
			                    +'<td><img class="product_img" src="'+em['thumbnail']+'"></td>'
			                    +'	<td style="padding-left: 10px;">'
			                    +'	<div>'
			                    +'		<div class="product_name">'+em['product_name']+'</div>'
			                    +'		<div class="star_div_web">	';
					
					for(var i=0;i<colored;i++){
						addStr += '<img class="product_starscore" src="/common/asset/images/ic_star_01.png">';
					}
					if(half_colored != 0){
						addStr += '<img class="product_starscore" src="/common/asset/images/ic_star_02.png">';
					}
					for(var i=0;i<non_colored;i++){
						addStr += '<img class="product_starscore" src="/common/asset/images/ic_star_00.png">';
					}
					
					addStr += '			<div class="product_score_web">'+em['rater']+'</div>'
								+'	</div>'
								+'	<div class="product_price">'+em['price']+'</div>'
								+'	</div>'
			                    +'</td>'
			                +'</tr>'
			            +'</table></a>'
			            +'<div class="seekTo_div">'
			            +'	<div class="seekTo_bar"></div>'
			            +'		<a onclick="seek('+em['starting_point']+')"><div class="fill-1">'
			            +'			<img src="/common/asset/images/fill-1.png">'
			            +'		</div>'
			            +'	<div class="seekTo_time">'+min+':'+sec+'</div></a>'
			            +'</div>'
			        +'</td></tr>';
					
					///addMobileStr
					addMobileStr += '<tr><td>';
					if(idx > 0){
						addMobileStr += '<div class="seekTo_bar"></div>';
					}
					addMobileStr += '<div class="seekTo_div">'
						+'		<a onclick="seek('+em['starting_point']+')"><div class="fill-1">'
			            +'			<img src="/common/asset/images/fill-1.png">'
			            +'		</div>'
			            +'	<div class="seekTo_time">'+min+':'+sec+'</div></a>'
			            +'	<div class="seekTo_bar"></div>'
			            +'<a href="'+em['url']+'" target="_blank">	'
		                +'<table class="item_list_innertable">	'
		                    +'<tr>	'
		                    +'<td><img class="product_img" src="'+em['thumbnail']+'"></td>'
		                    +'	<td style="padding-left: 10px;">'
		                    +'	<div>'
		                    +'		<div class="product_name">'+em['product_name']+'</div>'
		                    +'		<div class="star_div_mobile">	';
				
				for(var i=0;i<colored;i++){
					addMobileStr += '<img class="product_starscore" src="/common/asset/images/ic_star_01.png">';
				}
				if(half_colored != 0){
					addMobileStr += '<img class="product_starscore" src="/common/asset/images/ic_star_02.png">';
				}
				for(var i=0;i<non_colored;i++){
					addMobileStr += '<img class="product_starscore" src="/common/asset/images/ic_star_00.png">';
				}
				
				addMobileStr += '			<div class="product_score_mobile">'+toComma(em['rater'])+'</div>'
							+'	</div>'
							+'	<div class="product_price">'+em['price']+'</div>'
							+'	</div>'
		                    +'</td>'
		                +'</tr>'
		            +'</table></a>'
		            +'</td></tr>';
					
				});
			}
			
			$(".item_list_table").append(addStr);
			$(".item_list_table_mobile").append(addMobileStr);
		},error : function (e){
			console.log(e);
  		}
	});
	
}

function getProductReview(array){
	
	/*$.ajax({
		dataType :"json",
		async:true,
		crossDomain:true,
		method:"POST",
		url:"/getProductReviews.ajax",
		processData : false, 
		data:dataParam,
		contentType: "application/json;charset=UTF-8",
		success: function(resultMap){

			var result = resultMap['itemList'];
			
			var colored = 0;
			var non_colored = 0;
			var half_colored = 0;
			
			var valStr='';
			var valStr2='';
			
			if(result.length != 0){
				$(result).each(function(idx, em){
					if(em['has_review'] == true){
						colored = parseInt(em['rating']);
						non_colored = parseInt(5-em['rating']);
						half_colored = em['rating'] % 1;
						valStr = '';
						for(var i=0;i<colored;i++){
							valStr += '<img class="product_starscore" src="/common/asset/images/ic_star_01.png">';
						}
						if(half_colored != 0){
							valStr += '<img class="product_starscore" src="/common/asset/images/ic_star_02.png">';
						}
						for(var i=0;i<non_colored;i++){
							valStr += '<img class="product_starscore" src="/common/asset/images/ic_star_00.png">';
						}
						valStr2 = valStr+' <div class="product_score_web">'+toComma(em['rater'])+'</div>'
						$(".star_div_web:eq("+idx+")").empty().append(valStr2);
						valStr2 = valStr+' <div class="product_score_mobile">'+toComma(em['rater'])+'</div>'
						$(".star_div_mobile:eq("+idx+")").empty().append(valStr2);
					}
				});
			}
		},error: function(e){
			console.log(e);
		}
	});*/
}

