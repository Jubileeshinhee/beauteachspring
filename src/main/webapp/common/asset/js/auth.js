var OAUTH2_CLIENT_ID = '832742495984-7aipb8b5cro515lrj26s56i52hl22ug7.apps.googleusercontent.com';
var OAUTH2_SCOPES = [
	'https://www.googleapis.com/auth/youtube.force-ssl'
];

var channelId = '';
var thumbnails = '';
var channelName = '';

googleApiClientReady = function() {
	gapi.auth.init(function() {
		window.setTimeout(checkAuth, 1);
	});
}

function checkAuth() {
	gapi.auth.authorize({
		client_id: OAUTH2_CLIENT_ID,
		scope: OAUTH2_SCOPES,
		immediate: true
	}, handleAuthResult);
}

function handleAuthResult(authResult) {
	
	if (authResult && !authResult.error) {
		// Authorization was successful.
		loadAPIClientInterfaces();
		
		$(".comment_write_login").show();
		$(".comment_write_nonLogin").hide();
	} else {
		// Make the #login-link clickable. Attempt a non-immediate OAuth 2.0
		// client flow. The current function is called when that flow completes.
		$(".comment_write_nonLogin").show();
		$(".comment_write_login").hide();
		$('#login-link').click(function() {
			gapi.auth.authorize({
				client_id: OAUTH2_CLIENT_ID,
				scope: OAUTH2_SCOPES,
				immediate: false
			}, handleAuthResult);
		});
	}
}

function loadAPIClientInterfaces() {
	gapi.client.load('youtube', 'v3', function() {
		getUserId();
	});
}

function getUserId(){
	
	var request = gapi.client.youtube.channels.list({
		part:'id,snippet',
		mine:true
	});
	request.execute(function (response) {
		getUserThumbnails(response.items[0].id);
		channelName = response.items[0].snippet.title;
	});
}

function getUserThumbnails(getchannelId){
	
	channelId = getchannelId;
	
	var request = gapi.client.youtube.channels.list({
        part: 'snippet',
        id: getchannelId
    });
    request.execute(function (response) {
        var srchItems = response.result.items;
        $.each(srchItems, function (index, item) {
            $(".comment_write_img_area").attr('src',item.snippet.thumbnails.default.url);
            thumbnails = item.snippet.thumbnails.default.url;
        });
    });
}

function insertComment(){
	var videoId = $("#videoId").val();
	var textOriginal = $("#textOriginal").val();
	
	var request = gapi.client.youtube.commentThreads.insert({
        part: 'snippet',
        snippet:{
	        "topLevelComment" : {
	        	"snippet" : {
	        		"textOriginal":textOriginal,
	        		"videoId": videoId
	        	}
	        }
    	},
        alt: null
    });
    request.execute(function (response) {
        
        var resultStr = '<table class="comment-renderer-content"><tbody><tr><td rowspan="2" valign="top">	'
        	+'<a href="http://www.youtube.com/channel/'+channelId+'">	<img class="yt-thumb-square" src="'+thumbnails+'" onload=";window.__ytRIL &amp;&amp; __ytRIL(this)"></a></td>'
        	+'<td>	<div class="comment-renderer-header"><a href="http://www.youtube.com/channel/'+channelId+'">'+channelName+'</a>	'
        	+'<span class="comment-renderer-time" tabindex="0">1 week ago</span></div>	</td>	</tr>'
        	+'<tr><td>	<div class="comment-renderer-text" tabindex="0" role="article">	<div class="comment-renderer-text-content">'+textOriginal+'</div></div>	</td>	</tr>	</tbody></table>'
        	+'<div class="comment_middle_bar"></div>';
        $("#textOriginal").val('');
        $(".comment_area").prepend(resultStr);
    });
}